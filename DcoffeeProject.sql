--
-- File generated with SQLiteStudio v3.4.4 on Mon Oct 23 21:50:03 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
CREATE TABLE category (
    category_id   INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT    NOT NULL
);

INSERT INTO category (category_id, category_name) VALUES (1, 'กาแฟ');
INSERT INTO category (category_id, category_name) VALUES (2, 'ของหวาน');
INSERT INTO category (category_id, category_name) VALUES (3, 'candy');

-- Table: checkstock
CREATE TABLE checkstock (
    CRM_ID     INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    CRM_DATE   DATETIME DEFAULT ( (datetime('now', 'localtime') ) ),
    CRM_STATUS TEXT     NOT NULL,
    emp_id     INTEGER  REFERENCES employee (emp_id) ON DELETE RESTRICT
                                                     ON UPDATE CASCADE
                        NOT NULL
);

INSERT INTO checkstock (CRM_ID, CRM_DATE, CRM_STATUS, emp_id) VALUES (1, NULL, 'Y', 1);
INSERT INTO checkstock (CRM_ID, CRM_DATE, CRM_STATUS, emp_id) VALUES (2, NULL, 'Y', 1);

-- Table: checkstock_detail
CREATE TABLE checkstock_detail (
    CRD_ID        INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    RM_NAME       TEXT    NOT NULL,
    CRD_QTY       INTEGER NOT NULL,
    CRD_NOEXP     INTEGER NOT NULL,
    CRD_NODM      INTEGER NOT NULL,
    CRD_UNITPRICE REAL    NOT NULL,
    DAMAGE_VALUE  REAL    NOT NULL,
    EXPIRY_VALUE  REAL    NOT NULL,
    CRM_ID        INTEGER REFERENCES checkstock (CRM_ID) ON DELETE RESTRICT
                                                         ON UPDATE CASCADE
                          NOT NULL,
    RM_ID         INTEGER REFERENCES rawmaterial (RM_ID) ON DELETE RESTRICT
                                                         ON UPDATE CASCADE
                          NOT NULL
);

INSERT INTO checkstock_detail (CRD_ID, RM_NAME, CRD_QTY, CRD_NOEXP, CRD_NODM, CRD_UNITPRICE, DAMAGE_VALUE, EXPIRY_VALUE, CRM_ID, RM_ID) VALUES (1, 'น้ำตาล', 6, 0, 0, 34.0, 0.0, 0.0, 1, 3);

-- Table: employee
CREATE TABLE employee (
    emp_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    emp_login    TEXT (50) UNIQUE,
    emp_password TEXT (50) NOT NULL,
    emp_name     TEXT (50) NOT NULL,
    emp_role     INTEGER   NOT NULL,
    emp_gender   TEXT (3)  NOT NULL
);

INSERT INTO employee (emp_id, emp_login, emp_password, emp_name, emp_role, emp_gender) VALUES (1, 'manager', '12345678', 'Manager', 1, 'M');
INSERT INTO employee (emp_id, emp_login, emp_password, emp_name, emp_role, emp_gender) VALUES (2, 'employee1', '12345678', 'Nattiphan', 2, 'F');
INSERT INTO employee (emp_id, emp_login, emp_password, emp_name, emp_role, emp_gender) VALUES (3, 'employee2', '12345678', 'Jirapat', 2, 'M');
INSERT INTO employee (emp_id, emp_login, emp_password, emp_name, emp_role, emp_gender) VALUES (4, 'employee3', '12345678', 'Worawit', 2, 'F');
INSERT INTO employee (emp_id, emp_login, emp_password, emp_name, emp_role, emp_gender) VALUES (5, 'employee4', '12345678', 'Ricado', 2, 'F');

-- Table: product
CREATE TABLE product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       DOUBLE    NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    category_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (category_id) ON DELETE SET NULL
                                                                    ON UPDATE CASCADE
);

INSERT INTO product (product_id, product_name, product_price, product_size, product_sweet_level, product_type, category_id) VALUES (1, 'Espresso', 30.0, 'S', '1', 'H', 1);

-- Table: rawmaterial
CREATE TABLE rawmaterial (
    RM_ID      INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    RM_NAME    TEXT     NOT NULL,
    RM_MINIMUM INTEGER  NOT NULL,
    RM_INDATE  DATETIME DEFAULT ( (datetime('now', 'localtime') ) ) 
);

INSERT INTO rawmaterial (RM_ID, RM_NAME, RM_MINIMUM, RM_INDATE) VALUES (1, 'เมล็ดกาแฟ', 10, NULL);
INSERT INTO rawmaterial (RM_ID, RM_NAME, RM_MINIMUM, RM_INDATE) VALUES (2, 'นมสด', 10, NULL);
INSERT INTO rawmaterial (RM_ID, RM_NAME, RM_MINIMUM, RM_INDATE) VALUES (3, 'น้ำตาล', 10, NULL);
INSERT INTO rawmaterial (RM_ID, RM_NAME, RM_MINIMUM, RM_INDATE) VALUES (4, 'ไซรัป', 5, NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;