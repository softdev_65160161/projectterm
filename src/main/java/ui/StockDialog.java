/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package ui;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import model.CheckStock;
import model.CheckStockDetail;
import model.RawMaterial;
import service.CheckStockService;
import service.RawMaterialService;
import service.ValidateException;

/**
 *
 * @author 65160
 */
public class StockDialog extends javax.swing.JDialog {

    private RawMaterialService materialService = new RawMaterialService();
    private RawMaterial material;
    private CheckStock checkStock;
    private CheckStockService checkStockService;
    private ArrayList<RawMaterial> updatedRm = new ArrayList<>();
    private ArrayList<CheckStockDetail> list;
    private CheckStockDetail checkStockDetail;

    public StockDialog(java.awt.Frame parent, CheckStock checkStock, RawMaterial rawMaterial) {
        super(parent, true);
        initComponents();
        this.checkStock = checkStock;
        this.list = checkStock.getCheckStockDetails();
        this.checkStockService = new CheckStockService();
        this.material = new RawMaterial();
        this.materialService = new RawMaterialService();
        enableForm(false);

        System.out.println(list);

        tblCheckDetails.setModel(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return checkStock.getCheckStockDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckStockDetail checkDetail = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkDetail.getRawmaterialId();
                    case 1:
                        return checkDetail.getName();
                    case 2:
                        return checkDetail.getQty();
                    case 3:
                        return checkDetail.getNoEXP();
                    case 4:
                        return checkDetail.getNoDM();
                    case 5:
                        return checkDetail.getUnitPrice();
                    default:
                        return "";
                }
            }

            String[] colNames = {"Name", "Quantity", "NOEXP", "NODM", "UnitPrice"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

        });
    }

    public StockDialog(java.awt.Frame parent) {
        super(parent, true);
        initComponents();
        this.checkStock = new CheckStock();
        this.checkStockService = new CheckStockService();
        this.material = new RawMaterial();
        this.materialService = new RawMaterialService();
        enableForm(false);
    }

    private void setFormToObject() {
        if (material.getRawmaterialId() == -1) {
            return;
        }
        material.setQoh(Integer.parseInt(txtQoh.getText()));
        material.setUnitPrice(Float.parseFloat(txtUnitPrice.getText()));
        material.setMinimum(Integer.parseInt(txtMinimum.getText()));

    }

    private void setObjectToForm(String itemCode) {
        try {
            material = materialService.getById(Integer.parseInt(itemCode));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(this, "Please input ItemCode");
            return;
        }

        if (material == null) {
            resetForm();
            material = new RawMaterial();
            JOptionPane.showMessageDialog(this, "Not have this Material");
            return;
        }

        enableForm(true);
        txtItemCode.setText(String.valueOf(material.getRawmaterialId()));
        txtMinimum.setText(String.valueOf(material.getMinimum()));
        txtQoh.setText(String.valueOf(material.getQoh()));
        txtUnitPrice.setText(String.valueOf(material.getUnitPrice()));
        txtDamage.setText("0");
        txtEXP.setText("0");

    }

    private void enableForm(boolean status) {
        btnAdd.setEnabled(status);
        txtUnitPrice.setEnabled(status);
        txtDamage.setEnabled(status);
        txtMinimum.setEnabled(status);
        txtQoh.setEnabled(status);
        txtEXP.setEnabled(status);
    }

    public void resetForm() {
        txtItemCode.setText("");
        txtMinimum.setText("");
        txtQoh.setText("");
        txtUnitPrice.setText("");
        txtDamage.setText("");
        txtEXP.setText("");
        enableForm(false);
    }

    private void setObjectToForm() {
        txtItemCode.setText(String.valueOf(checkStockDetail.getRawmaterialId()));
        txtDamage.setText(String.valueOf(checkStockDetail.getDamageValue()));
        txtEXP.setText(String.valueOf(String.valueOf(checkStockDetail.getNoEXP())));
        txtMinimum.setText(String.valueOf(material.getMinimum()));
        txtUnitPrice.setText(String.valueOf(checkStockDetail.getUnitPrice()));
        txtQoh.setText(String.valueOf(checkStockDetail.getQty()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lblItemName = new javax.swing.JLabel();
        txtItemCode = new javax.swing.JTextField();
        lblQoh = new javax.swing.JLabel();
        lblUnitPrice = new javax.swing.JLabel();
        txtUnitPrice = new javax.swing.JTextField();
        lblMinimum = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtMinimum = new javax.swing.JTextField();
        txtQoh = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        btnFind = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblCheckDetails = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        lblQoh1 = new javax.swing.JLabel();
        txtDamage = new javax.swing.JTextField();
        lblMinimum1 = new javax.swing.JLabel();
        txtEXP = new javax.swing.JTextField();
        btnDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lblItemName.setText("Item Code :");

        txtItemCode.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N
        txtItemCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtItemCodeActionPerformed(evt);
            }
        });

        lblQoh.setText("Quantity on Hand :");

        lblUnitPrice.setText("Unit Price :");

        txtUnitPrice.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N
        txtUnitPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUnitPriceActionPerformed(evt);
            }
        });

        lblMinimum.setText("Minimum :");

        txtMinimum.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N

        txtQoh.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClear.setText("Cancle");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        btnFind.setText("Find");
        btnFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindActionPerformed(evt);
            }
        });

        tblCheckDetails.setFont(new java.awt.Font("TH Sarabun New", 0, 16)); // NOI18N
        tblCheckDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Quantity", "NOEXP", "NODM", "UnitPrice"
            }
        ));
        jScrollPane1.setViewportView(tblCheckDetails);

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        lblQoh1.setText("Damage :");

        txtDamage.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N

        lblMinimum1.setText("EXP :");

        txtEXP.setFont(new java.awt.Font("TH Sarabun New", 0, 14)); // NOI18N

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnDelete)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSave)
                .addGap(12, 12, 12)
                .addComponent(btnClear)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnAdd)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblItemName)
                                    .addComponent(lblUnitPrice))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtUnitPrice, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                    .addComponent(txtItemCode, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(btnFind)
                                        .addGap(37, 37, 37)
                                        .addComponent(lblQoh))
                                    .addComponent(lblMinimum))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtQoh, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(31, 31, 31)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblQoh1)
                                    .addComponent(lblMinimum1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDamage, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtEXP, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblItemName)
                            .addComponent(txtItemCode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblQoh)
                            .addComponent(txtQoh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnFind))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblUnitPrice)
                            .addComponent(txtUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblMinimum)
                            .addComponent(txtMinimum, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblQoh1)
                            .addComponent(txtDamage, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblMinimum1)
                            .addComponent(txtEXP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(129, 129, 129)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 348, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addComponent(btnAdd)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave)
                    .addComponent(btnClear)
                    .addComponent(btnDelete))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtUnitPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUnitPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUnitPriceActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (checkStock.getCheckStockDetails().isEmpty()) {
            dispose();
            return;
        }

        System.out.println("In btnSave");
        System.out.println(checkStock);
        System.out.println("-----------");

        checkStockService.addNew(checkStock);
        RawMaterialService rs = new RawMaterialService();

        for (RawMaterial rm : updatedRm) {
            try {
                rs.update(rm);
            } catch (ValidateException ex) {
                Logger.getLogger(StockDialog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        dispose();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        dispose();
    }//GEN-LAST:event_btnClearActionPerformed

    private void txtItemCodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtItemCodeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtItemCodeActionPerformed

    private void btnFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindActionPerformed
        String itemCode = txtItemCode.getText();
        setObjectToForm(itemCode);
    }//GEN-LAST:event_btnFindActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        if (material != null) {

            for (RawMaterial m : updatedRm) {
                if (material.getRawmaterialId() == m.getRawmaterialId()) {
                    resetForm();
                    JOptionPane.showMessageDialog(this, "this material already checked.");
                    return;
                }
            }

            setFormToObject();
            updatedRm.add(material);

            checkStock.addCheckStockDetail(material, Integer.parseInt(txtQoh.getText()), Integer.parseInt(txtEXP.getText()), Integer.parseInt(txtDamage.getText()));
            list = checkStock.getCheckStockDetails();

            System.out.println("checkStockDetail in btnAdd");
            System.out.println(list);
            System.out.println("-------------");

            tblCheckDetails.setModel(new AbstractTableModel() {
                String[] colNames = {"Id", "Name", "Quantity", "NOEXP", "NODM", "UnitPrice"};

                @Override
                public String getColumnName(int column) {
                    return colNames[column];
                }

                @Override
                public int getRowCount() {
                    return list.size();
                }

                @Override
                public int getColumnCount() {
                    return 6;
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    CheckStockDetail checkDetail = list.get(rowIndex);
                    switch (columnIndex) {
                        case 0:
                            return checkDetail.getRawmaterialId();
                        case 1:
                            return checkDetail.getName();
                        case 2:
                            return checkDetail.getQty();
                        case 3:
                            return checkDetail.getNoEXP();
                        case 4:
                            return checkDetail.getNoDM();
                        case 5:
                            return checkDetail.getUnitPrice();
                        default:
                            return "";
                    }
                }
            });
        } else {
            return;
        }
        resetForm();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblCheckDetails.getSelectedRow();

        if (selectedIndex < 0) {
            return;
        }

        try {
            list.remove(selectedIndex);
            updatedRm.remove(selectedIndex);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return;
        }

        refrestTable();
    }//GEN-LAST:event_btnDeleteActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnFind;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblItemName;
    private javax.swing.JLabel lblMinimum;
    private javax.swing.JLabel lblMinimum1;
    private javax.swing.JLabel lblQoh;
    private javax.swing.JLabel lblQoh1;
    private javax.swing.JLabel lblUnitPrice;
    private javax.swing.JTable tblCheckDetails;
    private javax.swing.JTextField txtDamage;
    private javax.swing.JTextField txtEXP;
    private javax.swing.JTextField txtItemCode;
    private javax.swing.JTextField txtMinimum;
    private javax.swing.JTextField txtQoh;
    private javax.swing.JTextField txtUnitPrice;
    // End of variables declaration//GEN-END:variables

    private void refrestTable() {
        list = checkStock.getCheckStockDetails();
        tblCheckDetails.revalidate();
        tblCheckDetails.repaint();
    }
}
