/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package ui;

import dao.CheckStockDetailDao;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import model.CheckStock;
import model.CheckStockDetail;
import model.Employee;
import model.OrderBill;
import model.OrderBillDetail;
import model.RawMaterial;
import service.CheckStockService;
import service.EmployeeService;
import service.OrderBillService;
import service.RawMaterialService;

/**
 *
 * @author 65160
 */
public class StockPanel extends javax.swing.JPanel {

    private final RawMaterialService materialService;
    private ArrayList<RawMaterial> materials;
    private ArrayList<RawMaterial> lowMaterials;
    private RawMaterial material;
    private final CheckStockService checkStockService;
    private ArrayList checkStocks;
    private CheckStock checkStock;
    private ArrayList<CheckStockDetail> checkStockDetails;
    private final OrderBillService orderBillService;
    private ArrayList orderBillDetails;
    private OrderBill orderBill;
    private OrderBillDetail orderBillDetail;
    private List<OrderBill> orderBills;
    private ArrayList<OrderBillDetail> listOrd;
    private EmployeeService empService = new EmployeeService();
    private Employee emp = empService.getCurrentUser();

    /**
     * Creates new form StockPanel
     */
    public StockPanel() {
        initComponents();

        // have to set Locale
        Locale.setDefault(Locale.ENGLISH);
        enableFormImport(false);
        //
        checkStockService = new CheckStockService();
        materialService = new RawMaterialService();
        orderBillService = new OrderBillService();

        materials = new ArrayList<>();
        lowMaterials = new ArrayList<>();
        checkStocks = new ArrayList<>();
        orderBillDetails = new ArrayList();

        orderBill = new OrderBill(emp);
        materials = materialService.getAll();
        lowMaterials = materialService.getLowItem();
        checkStocks = (ArrayList) checkStockService.getCheckStocks();
        orderBills = orderBillService.getOrderBills();

        listOrd = new ArrayList<>();

        reTableStock();
        reTableLowStock();

        tblCheckStockDetails.setModel(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return checkStocks.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckStock checkStock = (CheckStock) checkStocks.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return checkStock.getCheckId();
                    case 1:
                        return checkStock.getCheckDate();
                    case 2:
                        return checkStock.getEmployee().getName();
                    default:
                        return "Unknown";
                }
            }

            String[] colNames = {"Id", "Date", "Emp Name"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

        });

        tblBills.setModel(new AbstractTableModel() {
            String[] columnOrderBill = {"Order Bill ID", "Total", "Tax", "Date", "Vendor", "Emp ID"};

            @Override
            public String getColumnName(int column) {
                return columnOrderBill[column];
            }

            @Override
            public int getRowCount() {
                return orderBills.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                OrderBill orderBill = (OrderBill) orderBills.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return orderBill.getId();
                    case 1:
                        return orderBill.getTotal();
                    case 2:
                        return orderBill.getTax();
                    case 3:
                        return orderBill.getOrderDate();
                    case 4:
                        return orderBill.getVendorName();
                    case 5:
                        return orderBill.getEmployee().getName();
                    default:
                        return "Unknown";
                }
            }
        });

    }

    private void reTableLowStock() {
        tblLowItem.setModel(new AbstractTableModel() {
            String[] columnName = {"Item Code", "Item Name", "Price", "Quantity", "Minimum", "In-Date"};

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return lowMaterials.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RawMaterial material = lowMaterials.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getRawmaterialId();
                    case 1:
                        return material.getRawmaterialName();
                    case 2:
                        return material.getUnitPrice();
                    case 3:
                        return material.getQoh();
                    case 4:
                        return material.getMinimum();
                    case 5:
                        return material.getIndate();
                    default:
                        return "Unknown";
                }
            }
        });
    }

    private void reTableStock() {
        tblItem.setModel(new AbstractTableModel() {
            String[] columnName = {"Item Code", "Item Name", "Price", "Quantity", "Minimum", "In-Date"};

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return materials.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RawMaterial material = materials.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return material.getRawmaterialId();
                    case 1:
                        return material.getRawmaterialName();
                    case 2:
                        return material.getUnitPrice();
                    case 3:
                        return material.getQoh();
                    case 4:
                        return material.getMinimum();
                    case 5:
                        return material.getIndate();
                    default:
                        return "Unknown";
                }
            }
        });
    }

    private void searchTableStock() {
        tblItem.setModel(new AbstractTableModel() {
            String[] columnName = {"Item Code", "Item Name", "Price", "Quantity", "Minimum", "In-Date"};

            @Override
            public String getColumnName(int column) {
                return columnName[column];
            }

            @Override
            public int getRowCount() {
                return 1;
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                RawMaterial Item = material;
                switch (columnIndex) {
                    case 0:
                        return material.getRawmaterialId();
                    case 1:
                        return material.getRawmaterialName();
                    case 2:
                        return material.getUnitPrice();
                    case 3:
                        return material.getQoh();
                    case 4:
                        return material.getMinimum();
                    case 5:
                        return material.getIndate();
                    default:
                        return "Unknown";
                }
            }
        });
    }

    private void openDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        StockDialog stockDialog = new StockDialog(frame, checkStock, material);
        stockDialog.setLocationRelativeTo(this);
        stockDialog.setVisible(true);
        stockDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    private void openAddMaterialDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        AddMaterialDialog addMaterialDialog = new AddMaterialDialog(frame, material);
        addMaterialDialog.setLocationRelativeTo(this);
        addMaterialDialog.setVisible(true);
        addMaterialDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }
        });
    }

    private void refreshTable() {
        materials = materialService.getAll();
        lowMaterials = materialService.getLowItem();
        checkStocks = (ArrayList) checkStockService.getCheckStocks();
        orderBills = (ArrayList) orderBillService.getOrderBills();
        tblItem.revalidate();
        tblItem.repaint();
        tblLowItem.revalidate();
        tblLowItem.repaint();
        tblCheckStockDetails.revalidate();
        tblCheckStockDetails.repaint();
        tblBills.revalidate();
        tblBills.repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jProgressBar1 = new javax.swing.JProgressBar();
        CheckStockPanel = new javax.swing.JTabbedPane();
        StockPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        lblItemName = new javax.swing.JLabel();
        txtSearchItem = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();
        btnRefresh = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblItem = new javax.swing.JTable();
        btnPrint = new javax.swing.JButton();
        lblStock = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        LowStockPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblLowItem = new javax.swing.JTable();
        btnLowPrint = new javax.swing.JButton();
        lblLowStock = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        btnView = new javax.swing.JButton();
        btnCheckStock = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtCheckStockId = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtInspectorName = new javax.swing.JLabel();
        btnDelCheckStocks = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblCheckStockDetails = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblInspector = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tblImport = new javax.swing.JTable();
        jLabel22 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        btnAddImport = new javax.swing.JButton();
        btnDeleteImport = new javax.swing.JButton();
        lblTotal3 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        txtQuatity = new javax.swing.JTextField();
        txtPriceUnit = new javax.swing.JTextField();
        txtVendorImport = new javax.swing.JTextField();
        btnSubmit = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        txtTotal = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblBills = new javax.swing.JTable();
        lblOderBillID = new javax.swing.JLabel();
        btnViewBill = new javax.swing.JButton();
        btnEditBill = new javax.swing.JButton();
        btnDeleteBill = new javax.swing.JButton();
        lblOderer = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblBillDetails = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();

        CheckStockPanel.setBackground(new java.awt.Color(255, 255, 255));
        CheckStockPanel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        CheckStockPanel.setOpaque(true);

        StockPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblItemName.setText("Item Name");

        txtSearchItem.setFont(new java.awt.Font("TH Sarabun New", 0, 12)); // NOI18N

        btnSearch.setText("Search");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        btnRefresh.setText("Refresh");
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        tblItem.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tblItem);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 1062, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblItemName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtSearchItem, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnRefresh)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblItemName, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSearchItem, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch)
                    .addComponent(btnRefresh))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnPrint.setText("Print");
        btnPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintActionPerformed(evt);
            }
        });

        lblStock.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblStock.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblStock.setText("Stock");

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout StockPanelLayout = new javax.swing.GroupLayout(StockPanel);
        StockPanel.setLayout(StockPanelLayout);
        StockPanelLayout.setHorizontalGroup(
            StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StockPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(StockPanelLayout.createSequentialGroup()
                        .addComponent(lblStock)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(StockPanelLayout.createSequentialGroup()
                        .addComponent(btnAdd)
                        .addGap(18, 18, 18)
                        .addComponent(btnDelete)
                        .addGap(18, 18, 18)
                        .addComponent(btnEdit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPrint)))
                .addContainerGap())
        );
        StockPanelLayout.setVerticalGroup(
            StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, StockPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(lblStock)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(17, 17, 17)
                .addGroup(StockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnDelete)
                    .addComponent(btnEdit)
                    .addComponent(btnPrint))
                .addGap(74, 74, 74))
        );

        CheckStockPanel.addTab("Available Stock", StockPanel);

        LowStockPanel.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblLowItem.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tblLowItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tblLowItem);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1062, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 432, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnLowPrint.setText("Print");
        btnLowPrint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLowPrintActionPerformed(evt);
            }
        });

        lblLowStock.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblLowStock.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLowStock.setText("Low Stock");

        javax.swing.GroupLayout LowStockPanelLayout = new javax.swing.GroupLayout(LowStockPanel);
        LowStockPanel.setLayout(LowStockPanelLayout);
        LowStockPanelLayout.setHorizontalGroup(
            LowStockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(LowStockPanelLayout.createSequentialGroup()
                .addGroup(LowStockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LowStockPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnLowPrint))
                    .addGroup(LowStockPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(LowStockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(LowStockPanelLayout.createSequentialGroup()
                                .addComponent(lblLowStock)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        LowStockPanelLayout.setVerticalGroup(
            LowStockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, LowStockPanelLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(lblLowStock)
                .addGap(18, 18, 18)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnLowPrint)
                .addGap(54, 54, 54))
        );

        CheckStockPanel.addTab("Low Stock", LowStockPanel);

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Check Stock");

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        btnCheckStock.setText("Check Stock");
        btnCheckStock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckStockActionPerformed(evt);
            }
        });

        jLabel4.setText("CheckStock ID:");

        jLabel5.setText("Inspector: ");

        btnDelCheckStocks.setText("Delete");
        btnDelCheckStocks.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelCheckStocksActionPerformed(evt);
            }
        });

        tblCheckStockDetails.setFont(new java.awt.Font("TH Sarabun New", 0, 16)); // NOI18N
        tblCheckStockDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblCheckStockDetails);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 423, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(btnView)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDelCheckStocks)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnCheckStock, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCheckStockId, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtInspectorName, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCheckStockId)
                    .addComponent(jLabel5)
                    .addComponent(txtInspectorName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnView, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnCheckStock)
                    .addComponent(btnDelCheckStocks, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Details");

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblInspector.setFont(new java.awt.Font("TH Sarabun New", 0, 16)); // NOI18N
        tblInspector.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblInspector);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText(" Inspector");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(52, 52, 52))
        );

        CheckStockPanel.addTab("Check Stock", jPanel3);

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));

        tblImport.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        tblImport.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Name", "Quantity", "UnitPrice", "Amount"
            }
        ));
        jScrollPane8.setViewportView(tblImport);

        jLabel22.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        jLabel22.setText("ID :");

        jLabel24.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        jLabel24.setText("Vendor :");

        jLabel26.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        jLabel26.setText("Quatity :");

        jLabel27.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        jLabel27.setText("Price/Unit :");

        btnAddImport.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        btnAddImport.setText("Add");
        btnAddImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddImportActionPerformed(evt);
            }
        });

        btnDeleteImport.setFont(new java.awt.Font("TH Sarabun New", 0, 18)); // NOI18N
        btnDeleteImport.setText("Delete");
        btnDeleteImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteImportActionPerformed(evt);
            }
        });

        lblTotal3.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        lblTotal3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal3.setText("Total : ");

        txtId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIdActionPerformed(evt);
            }
        });

        txtQuatity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQuatityActionPerformed(evt);
            }
        });

        txtPriceUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceUnitActionPerformed(evt);
            }
        });

        txtVendorImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtVendorImportActionPerformed(evt);
            }
        });

        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        txtTotal.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txtTotal.setText("0.0");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 1059, Short.MAX_VALUE)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel10Layout.createSequentialGroup()
                                        .addComponent(lblTotal3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(28, 28, 28))
                                    .addGroup(jPanel10Layout.createSequentialGroup()
                                        .addComponent(btnAddImport)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnDeleteImport))
                                    .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(23, 23, 23))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(232, 232, 232)
                                .addComponent(jLabel26)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtQuatity, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel27)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPriceUnit, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel10Layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel10Layout.createSequentialGroup()
                                        .addComponent(jLabel22)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel10Layout.createSequentialGroup()
                                        .addComponent(jLabel24)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtVendorImport, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel24)
                    .addComponent(txtVendorImport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel10Layout.createSequentialGroup()
                                .addGap(128, 128, 128)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnAddImport)
                                    .addComponent(btnDeleteImport))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel26)
                                    .addComponent(txtQuatity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel27)
                                    .addComponent(txtPriceUnit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(114, 114, 114)))
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblTotal3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(17, 17, 17)
                        .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        CheckStockPanel.addTab("Import", jPanel10);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));
        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblBills.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tblBills);

        lblOderBillID.setText("Order Bill ID : ");

        btnViewBill.setText("View");
        btnViewBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewBillActionPerformed(evt);
            }
        });

        btnEditBill.setText("Edit");
        btnEditBill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditBillActionPerformed(evt);
            }
        });

        btnDeleteBill.setText("Delete");

        lblOderer.setText("Name : ");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 467, Short.MAX_VALUE)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(btnViewBill)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEditBill)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnDeleteBill))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(lblOderBillID, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(91, 91, 91)
                                .addComponent(lblOderer, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblOderBillID)
                    .addComponent(lblOderer))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnViewBill)
                    .addComponent(btnEditBill)
                    .addComponent(btnDeleteBill))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));
        jPanel8.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        tblBillDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(tblBillDetails);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6)
                .addContainerGap())
        );

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setText("Order Bills");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Order Bills");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Details");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 558, Short.MAX_VALUE)
                        .addGap(25, 25, 25))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel6)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(jLabel6)
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        CheckStockPanel.addTab("Order Bills", jPanel6);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(CheckStockPanel, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(CheckStockPanel)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        reTableStock();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        material = materialService.getByName(txtSearchItem.getText());
        if (material == null) {
            reTableStock();
        } else {
            searchTableStock();
        }
    }//GEN-LAST:event_btnSearchActionPerformed

    private void btnCheckStockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckStockActionPerformed
        checkStock = new CheckStock(emp);
        openDialog();
    }//GEN-LAST:event_btnCheckStockActionPerformed

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        int selectedIndex = tblCheckStockDetails.getSelectedRow();
        System.out.println(selectedIndex);
        if (selectedIndex < 0) {
            return;
        }

        try {
            checkStock = (CheckStock) checkStocks.get(selectedIndex);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            resetInspectorTable();
            return;
        }

        int checkId = checkStock.getCheckId();
        txtCheckStockId.setText(String.valueOf(checkId));
        txtInspectorName.setText(checkStock.getEmployee().getName());
        checkStock = checkStockService.getById(checkId);
        checkStockDetails = checkStock.getCheckStockDetails();

        if (selectedIndex >= 0) {
            tblInspector.setModel(new AbstractTableModel() {
                @Override
                public int getRowCount() {
                    return checkStockDetails.size();
                }

                @Override
                public int getColumnCount() {
                    return 7;
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    CheckStockDetail checkStockDetail = checkStockDetails.get(rowIndex);
                    switch (columnIndex) {
                        case 0:
                            return checkStockDetail.getName();
                        case 1:
                            return checkStockDetail.getQty();
                        case 2:
                            return checkStockDetail.getNoDM();
                        case 3:
                            return checkStockDetail.getNoEXP();
                        case 4:
                            return checkStockDetail.getUnitPrice();
                        case 5:
                            return checkStockDetail.getDamageValue();
                        case 6:
                            return checkStockDetail.getExpiryValue();
                        default:
                            return "";
                    }
                }

                String[] colNames = {"Name", "Quantity", "NODM", "NOEXP", "Price", "DamageValue", "ExpiryValue"};

                @Override
                public String getColumnName(int column) {
                    return colNames[column];
                }

            });
        }
        refreshTable();
    }//GEN-LAST:event_btnViewActionPerformed

    public void resetInspectorTable() {
        txtCheckStockId.setText("");
        txtInspectorName.setText("");
        tblInspector.setModel(new DefaultTableModel());
    }

    public void resetOrderBillDetailTable() {
        lblOderBillID.setText("");
        lblOderer.setText("");
        tblBillDetails.setModel(new DefaultTableModel());
    }

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        orderBillDetail = new OrderBillDetail();

        try {
            setImportFormToObject();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            JOptionPane.showMessageDialog(this, "invalid!!!");
            return;
        }

        int materialId = orderBillDetail.getRawMaterialId();
        material = materialService.getById(materialId);
        if (material == null) {
            txtId.setText("");
            txtQuatity.setText("");
            txtPriceUnit.setText("");
            JOptionPane.showMessageDialog(this, "Not have this Id");
            return;
        }

        //Set same Item into one row //
        int index = -1;
        for (OrderBillDetail obd : listOrd) {
            if (obd.getRawMaterialId() == orderBillDetail.getRawMaterialId()) {
                obd.setNet(obd.getNet() + orderBillDetail.getNet());
                obd.setQty(obd.getQty() + orderBillDetail.getQty());

                index = listOrd.indexOf(obd);
                break;
            }
        }

        try {
            orderBillDetail = listOrd.get(index);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        if (index < 0) {
            orderBill.addOrderBillDetail(orderBillDetail);
            listOrd.add(orderBillDetail);
        } else {
            orderBill.updateOrderBillDetail(orderBillDetail, index);
        }
        //Set same Item into one row //

        orderBillDetails = orderBill.getOrderBillDetial();

        tblImport.setModel(new AbstractTableModel() {
            String[] colNames = {"ID", "Name", "Quantity", "UnitPrice", "Amount"};

            @Override
            public String getColumnName(int column) {
                return colNames[column];
            }

            @Override
            public int getRowCount() {
                return orderBillDetails.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                orderBillDetail = (OrderBillDetail) orderBillDetails.get(rowIndex);
                material = materialService.getById(orderBillDetail.getRawMaterialId());
                switch (columnIndex) {
                    case 0:
                        return orderBillDetail.getRawMaterialId();
                    case 1:
                        return material.getRawmaterialName();
                    case 2:
                        return orderBillDetail.getQty();
                    case 3:
                        return orderBillDetail.getUnitPrice();
                    case 4:
                        return orderBillDetail.getNet();
                    default:
                        return "";
                }
            }
        });
        txtTotal.setText(String.valueOf(orderBill.getTotal()));
        txtId.setText("");
        txtQuatity.setText("");
        txtPriceUnit.setText("");
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        if (!orderBill.getOrderBillDetial().isEmpty()) {
            orderBillService.addNew(orderBill);
        }
        txtVendorImport.setText("");
        txtId.setText("");
        txtQuatity.setText("");
        txtPriceUnit.setText("");
        txtTotal.setText("0.0");
        enableFormImport(false);
        btnAddImport.setEnabled(true);
        tblImport.setModel(new AbstractTableModel() {
            @Override
            public int getRowCount() {
                return 0;
            }

            @Override
            public int getColumnCount() {
                return 0;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return "";
            }
        });
        
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void txtVendorImportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtVendorImportActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtVendorImportActionPerformed

    private void txtPriceUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceUnitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPriceUnitActionPerformed

    private void txtQuatityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQuatityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuatityActionPerformed

    private void txtIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIdActionPerformed

    private void btnAddImportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddImportActionPerformed
        enableFormImport(true);
        btnAddImport.setEnabled(false);
        orderBill = new OrderBill(emp);
        orderBillDetail = new OrderBillDetail();
        listOrd = new ArrayList<>();
    }//GEN-LAST:event_btnAddImportActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        material = new RawMaterial();
        openAddMaterialDialog();
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = tblItem.getSelectedRow();
        if (selectedIndex >= 0) {

            try {
                material = materials.get(selectedIndex);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return;
            }

            materialService.delete(material);
        }

        refreshTable();

    }//GEN-LAST:event_btnDeleteActionPerformed

    private void btnDelCheckStocksActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelCheckStocksActionPerformed
        int selectedIndex = tblCheckStockDetails.getSelectedRow();

        System.out.println("Selected Index");
        System.out.println(selectedIndex);
        System.out.println("---------------");

        if (selectedIndex >= 0) {

            try {
                checkStock = (CheckStock) checkStocks.get(selectedIndex);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return;
            }

            int checkId = checkStock.getCheckId();
            checkStock = checkStockService.getById(checkId);
            checkStockService.delete(checkStock);

            CheckStockDetailDao csdDao = new CheckStockDetailDao();
            List<CheckStockDetail> list = csdDao.getAll("crm_id=" + checkId, "crm_id");
            for (CheckStockDetail csd : list) {
                csdDao.delete(csd);
            }
        }
        refreshTable();
    }//GEN-LAST:event_btnDelCheckStocksActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = tblItem.getSelectedRow();

        if (selectedIndex >= 0) {
            try {
                material = materials.get(selectedIndex);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                return;
            }
            openAddMaterialDialog();
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnViewBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewBillActionPerformed
        int selectedIndex = tblBills.getSelectedRow();
        System.out.println(selectedIndex);
        if (selectedIndex < 0) {
            return;
        }

        try {
            orderBill = (OrderBill) orderBills.get(selectedIndex);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            resetOrderBillDetailTable();
            return;
        }

        int OrderId = orderBill.getId();
        lblOderBillID.setText("Order Bill ID : " + String.valueOf(OrderId));
        lblOderer.setText("Name : " + orderBill.getEmployee().getName());
        orderBill = orderBillService.getById(OrderId);
        orderBillDetails = orderBill.getOrderBillDetial();

        if (selectedIndex >= 0) {
            tblBillDetails.setModel(new AbstractTableModel() {
                @Override
                public int getRowCount() {
                    return orderBillDetails.size();
                }

                @Override
                public int getColumnCount() {
                    return 7;
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    OrderBillDetail obd = (OrderBillDetail) orderBillDetails.get(rowIndex);
                    switch (columnIndex) {
                        case 0:
                            return obd.getId();
                        case 1:
                            return obd.getQty();
                        case 2:
                            return obd.getUnitPrice();
                        case 3:
                            return obd.getNet();
                        case 4:
                            return obd.getRawMaterialId();
                        case 5:
                            return obd.getOrderId();
                        default:
                            return "Unknow";
                    }
                }

                String[] colOrderNames = {"ID", "Quantity", "Price/Unit", "Net Income", "Raw Material ID", "Order ID"};

                @Override
                public String getColumnName(int column) {
                    return colOrderNames[column];
                }

            });
        }
        refreshTable();
    }//GEN-LAST:event_btnViewBillActionPerformed

    private void btnEditBillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditBillActionPerformed
        int selectedIndex = tblBills.getSelectedRow();
        if (selectedIndex >= 0) {
            orderBill = (OrderBill) orderBillDetails.get(selectedIndex);
            int OrderId = orderBill.getId();
            orderBill = orderBillService.getById(OrderId);

            System.out.println(checkStock);
            openDialog();
        }
    }//GEN-LAST:event_btnEditBillActionPerformed

    private void btnDeleteImportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteImportActionPerformed
        int selectedIndex = tblImport.getSelectedRow();
        if (selectedIndex < 0) {
            return;
        }
        try {
            orderBill.delOrderBillDetail((OrderBillDetail) orderBillDetails.get(selectedIndex));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return;
        }
        listOrd.remove(selectedIndex);
        txtTotal.setText(String.valueOf(orderBill.getTotal()));
        refreshImportTable();
    }//GEN-LAST:event_btnDeleteImportActionPerformed

    private void btnPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintActionPerformed
        materials = materialService.getAll();
        openDialogAllMaterial();
    }//GEN-LAST:event_btnPrintActionPerformed

    private void btnLowPrintActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLowPrintActionPerformed
        materials = materialService.getLowItem();
        openDialogAllMaterial();
    }//GEN-LAST:event_btnLowPrintActionPerformed

    public void setImportFormToObject() throws Exception {
        int qty = Integer.parseInt(txtQuatity.getText());
        float unitPrice = Float.parseFloat(txtPriceUnit.getText());
        orderBillDetail.setQty(qty);
        orderBillDetail.setRawMaterialId(Integer.parseInt(txtId.getText()));
        orderBillDetail.setUnitPrice(unitPrice);
        orderBillDetail.setNet(qty * unitPrice);
        orderBill.setVendorName(txtVendorImport.getText());
    }

    private void setObjectToImportForm() {
        txtId.setText(String.valueOf(orderBillDetail.getRawMaterialId()));
        txtQuatity.setText(String.valueOf(orderBillDetail.getQty()));
        txtPriceUnit.setText(String.valueOf(orderBillDetail.getUnitPrice()));
    }

    private void refreshImportTable() {
        tblImport.revalidate();
        tblImport.repaint();
    }

    public void enableFormImport(boolean status) {
        txtId.setEnabled(status);
        txtVendorImport.setEnabled(status);
        txtQuatity.setEnabled(status);
        txtPriceUnit.setEnabled(status);
        btnSave.setEnabled(status);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane CheckStockPanel;
    private javax.swing.JPanel LowStockPanel;
    private javax.swing.JPanel StockPanel;
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnAddImport;
    private javax.swing.JButton btnCheckStock;
    private javax.swing.JButton btnDelCheckStocks;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnDeleteBill;
    private javax.swing.JButton btnDeleteImport;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnEditBill;
    private javax.swing.JButton btnLowPrint;
    private javax.swing.JButton btnPrint;
    private javax.swing.JButton btnRefresh;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JButton btnView;
    private javax.swing.JButton btnViewBill;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JLabel lblItemName;
    private javax.swing.JLabel lblLowStock;
    private javax.swing.JLabel lblOderBillID;
    private javax.swing.JLabel lblOderer;
    private javax.swing.JLabel lblStock;
    private javax.swing.JLabel lblTotal3;
    private javax.swing.JTable tblBillDetails;
    private javax.swing.JTable tblBills;
    private javax.swing.JTable tblCheckStockDetails;
    private javax.swing.JTable tblImport;
    private javax.swing.JTable tblInspector;
    private javax.swing.JTable tblItem;
    private javax.swing.JTable tblLowItem;
    private javax.swing.JLabel txtCheckStockId;
    private javax.swing.JTextField txtId;
    private javax.swing.JLabel txtInspectorName;
    private javax.swing.JTextField txtPriceUnit;
    private javax.swing.JTextField txtQuatity;
    private javax.swing.JTextField txtSearchItem;
    private javax.swing.JLabel txtTotal;
    private javax.swing.JTextField txtVendorImport;
    // End of variables declaration//GEN-END:variables

    private void openDialogAllMaterial() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        AllMaterialsDialog allMaterialDialog = new AllMaterialsDialog(frame, materials);
        allMaterialDialog.setLocationRelativeTo(this);
        allMaterialDialog.setVisible(true);
    }

}
