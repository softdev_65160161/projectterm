/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MSiRTX
 */
public class Employee {
    private int id;
    private String login;
    private String password;
    private String name;
    private int role;
    private String gender;

    public Employee(int id, String login, String password, String name, int role, String gender) {
        this.id = id;
        this.login = login;
        this.password = password;
        this.name = name;
        this.role = role;
        this.gender = gender;
    }
    public Employee(String login, String password, String name, int role, String gender) {
        this.id = -1;
        this.login = login;
        this.password = password;
        this.name = name;
        this.role = role;
        this.gender = gender;
    }
    
    public Employee() {
        this(-1,"","","",0,"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", login=" + login + ", password=" + password + ", name=" + name + ", role=" + role + ", gender=" + gender + '}';
    }
    
    public static Employee fromRS(ResultSet rs) {
        Employee emp = new Employee();
        try {
            emp.setId(rs.getInt("emp_id"));
            emp.setLogin(rs.getString("emp_login"));
            emp.setPassword(rs.getString("emp_password"));
            emp.setName(rs.getString("emp_name"));
            emp.setRole(rs.getInt("emp_role"));
            emp.setGender(rs.getString("emp_gender"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return emp;
    }
     public boolean isValid() {
        return this.login.length() >= 3
                && this.name.length() >= 3
                && this.password.length() >= 8;

    }
}
