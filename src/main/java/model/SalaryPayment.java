/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class SalaryPayment {

    private int id;
    private Date payDate;
    private float amount;
    private String status;
    private int empId;
    private String name;
    private int role;

    public SalaryPayment(int id, Date payDate, float amount, String status, int empId) {
        this.id = id;
        this.payDate = payDate;
        this.amount = amount;
        this.status = status;
        this.empId = empId;
    }

    public SalaryPayment(Date payDate, float amount, String status, int empId) {
        this.id = -1;
        this.payDate = payDate;
        this.amount = amount;
        this.status = status;
        this.empId = empId;
    }

    public SalaryPayment(float amount, String status, int empId) {
        this.id = -1;
        this.amount = amount;
        this.status = status;
        this.empId = empId;
    }

    public SalaryPayment(float amount, int empId) {
        this.id = -1;
        this.amount = amount;
        this.status = "";
        this.empId = empId;
    }

    public SalaryPayment() {
        this.id = -1;
        this.payDate = null;
        this.amount = 0;
        this.status = "";
        this.empId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "SalaryPayment{" + "id=" + id + ", payDate=" + payDate + ", amount=" + amount + ", status=" + status + ", empId=" + empId + ", name=" + name + ", role=" + role + '}';
    }

    public static SalaryPayment fromRS(ResultSet rs) {
        SalaryPayment salaryPayment = new SalaryPayment();
        try {
            salaryPayment.setId(rs.getInt("sp_id"));
            salaryPayment.setPayDate(rs.getDate("sp_paydate"));
            salaryPayment.setAmount(rs.getFloat("sp_amount"));
            salaryPayment.setStatus(rs.getString("sp_status"));
            salaryPayment.setEmpId(rs.getInt("emp_id"));

            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(salaryPayment.getEmpId());
            String name = employee.getName();
            salaryPayment.setName(name);
            int rold = employee.getRole();
            salaryPayment.setRole(rold);

        } catch (SQLException ex) {
            Logger.getLogger(SalaryPayment.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salaryPayment;
    }
}
