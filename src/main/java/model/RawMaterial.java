/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tauru
 */
public class RawMaterial {

    private int rawmaterialId;
    private String rawmaterialName;
    private int minimum;
    private Date indate;
    private float unitPrice;
    private int qoh;

    public RawMaterial(int rawmaterialId, String rawmaterialName, int minimum, Date indate, float unitPrice, int qoh) {
        this.rawmaterialId = rawmaterialId;
        this.rawmaterialName = rawmaterialName;
        this.minimum = minimum;
        this.indate = indate;
        this.unitPrice = unitPrice;
        this.qoh = qoh;
    }

    public RawMaterial(String rawmaterialName, int minimum, Date indate, float unitPrice, int qoh) {
        this.rawmaterialId = -1;
        this.rawmaterialName = rawmaterialName;
        this.minimum = minimum;
        this.indate = indate;
        this.unitPrice = unitPrice;
        this.qoh = qoh;
    }

    public RawMaterial(String rawmaterialName, int minimum, float unitPrice, int qoh) {
        this.rawmaterialId = -1;
        this.rawmaterialName = rawmaterialName;
        this.minimum = minimum;
        this.indate = null;
        this.unitPrice = unitPrice;
        this.qoh = qoh;
    }

    public RawMaterial() {
        this.rawmaterialId = -1;
        this.rawmaterialName = "";
        this.minimum = 0;
        this.indate = null;
        this.unitPrice = 0;
        this.qoh = 0;
    }

    public int getRawmaterialId() {
        return rawmaterialId;
    }

    public void setRawmaterialId(int rawmaterialId) {
        this.rawmaterialId = rawmaterialId;
    }

    public String getRawmaterialName() {
        return rawmaterialName;
    }

    public void setRawmaterialName(String rawmaterialName) {
        this.rawmaterialName = rawmaterialName;
    }

    public int getMinimum() {
        return minimum;
    }

    public void setMinimum(int minimum) {
        this.minimum = minimum;
    }

    public Date getIndate() {
        return indate;
    }

    public void setIndate(Date indate) {
        this.indate = indate;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    @Override
    public String toString() {
        return "Rawmaterial{" + "rawmaterialId=" + rawmaterialId + ", rawmaterialName=" + rawmaterialName + ", minimum=" + minimum + ", indate=" + indate + ", unitPrice=" + unitPrice + ", qoh=" + qoh + '}';
    }

    public static RawMaterial fromRS(ResultSet rs) {
        RawMaterial rawmaterial = new RawMaterial();
        try {
            rawmaterial.setRawmaterialId(rs.getInt("rm_id"));
            rawmaterial.setRawmaterialName(rs.getString("rm_name"));
            rawmaterial.setMinimum(rs.getInt("rm_minimum"));
            rawmaterial.setIndate(rs.getTimestamp("rm_indate"));
            rawmaterial.setUnitPrice(rs.getFloat("rm_unit_price"));
            rawmaterial.setQoh(rs.getInt("rm_qoh"));

            //Population
//            CustomerDao customerDao = new CustomerDao();
//            UserDao userDao = new UserDao();
//            Customer customer = customerDao.get(checkStock.getCustomerId());
//            User user = userDao.get(checkStock.getUserId());
//            checkStock.setCustomer(customer);
//            checkStock.setUser(user);
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return rawmaterial;
    }

    public boolean isValid() {
//Name >= 2
//        Qoh > 0
//        UnitPrice > 0
//        Minimum > 5
        return this.rawmaterialName.length() >= 2
                && this.qoh > 0
                && this.unitPrice > 0
                && this.minimum > 0;
    }
}
