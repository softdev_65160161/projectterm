/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nutta
 */
public class Promotion {

    private int id;
    private String name;
    private String condition;
    private float discountRate;
    private String startDate;
    private String endDate;
    private int status;

    public Promotion(int id, String name, String condition, float discountRate, String startDate, String endDate, int status) {
        this.id = id;
        this.name = name;
        this.condition = condition;
        this.discountRate = discountRate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public Promotion(String name, String condition, float discountRate, String startDate, String endDate, int status) {
        this.id = -1;
        this.name = name;
        this.condition = condition;
        this.discountRate = discountRate;
        this.startDate = startDate;
        this.endDate = endDate;
        this.status = status;
    }

    public Promotion() {
        this.id = -1;
        this.name = "";
        this.condition = "";
        this.discountRate = 0;
        this.startDate = null;
        this.endDate = null;
        this.status = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public float getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(float discountRate) {
        this.discountRate = discountRate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", condition=" + condition + ", discountRate=" + discountRate + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status + '}';
    }
   
    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("promo_id"));
            promotion.setName(rs.getString("promo_name"));
            promotion.setCondition(rs.getString("promo_con"));
            promotion.setDiscountRate(rs.getFloat("discount_rate"));
            promotion.setStartDate(rs.getString("promo_startdate"));
            promotion.setEndDate(rs.getString("promo_enddate"));
            promotion.setStatus(rs.getInt("promo_status"));

        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

}
