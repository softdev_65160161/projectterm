/*
 * Click nbfs://nbhost/SystemFileSystem/TorderBilllates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/TorderBilllates/Classes/Class.java to edit this torderBilllate
 */
package model;

import dao.EmployeeDao;
import dao.VendorDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class OrderBill {

    private int id;
    private float total;
    private Date orderDate;
    private float tax;
    private String vendorName;
    private int empId;
    private Employee employee;
    private ArrayList<OrderBillDetail> orderBillDetails = new ArrayList<>();

    public OrderBill(int id, float total, Date orderDate, float tax, String vendorName, int empId) {
        this.id = id;
        this.total = total;
        this.orderDate = orderDate;
        this.tax = tax;
        this.vendorName = vendorName;
        this.empId = empId;
    }

    public OrderBill(float total, Date orderDate, float tax, String vendorName, int empId) {
        this.id = -1;
        this.total = total;
        this.orderDate = orderDate;
        this.tax = tax;
        this.vendorName = vendorName;
        this.empId = empId;
    }

    public OrderBill(float total, float tax, String vendorName, int empId) {
        this.id = -1;
        this.total = total;
        this.orderDate = null;
        this.tax = tax;
        this.vendorName = vendorName;
        this.empId = empId;
    }

    public OrderBill(float tax, String vendorName, int empId) {
        this.id = -1;
        this.total = 0;
        this.orderDate = null;
        this.tax = tax;
        this.vendorName = vendorName;
        this.empId = empId;
    }

    public OrderBill() {
        this.id = -1;
        this.total = 0;
        this.orderDate = null;
        this.tax = 0;
        this.vendorName = "";
        this.empId = -1;
    }
    
    public OrderBill(Employee emp) {
        this.id = -1;
        this.total = 0;
        this.orderDate = null;
        this.tax = 0;
        this.vendorName = "";
        this.empId = emp.getId();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public float getTax() {
        return tax;
    }

    public void setTax(float tax) {
        this.tax = tax;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public ArrayList<OrderBillDetail> getOrderBillDetial() {
        return orderBillDetails;
    }

    public void setOrderBillDetial(ArrayList<OrderBillDetail> orderBillDetial) {
        this.orderBillDetails = orderBillDetial;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.empId = employee.getId();
    }

    @Override
    public String toString() {
        return "OrderBill{" + "id=" + id + ", total=" + total + ", orderDate=" + orderDate + ", tax=" + tax + ", vendorName=" + vendorName + ", empId=" + empId + ", employee=" + employee + ", orderBillDetails=" + orderBillDetails + '}';
    }

    public void addOrderBillDetail(OrderBillDetail orderBillDetail) {
        orderBillDetails.add(orderBillDetail);
        calculateTotal();
    }
    
    public void updateOrderBillDetail(OrderBillDetail orderBillDetail, int index) {
        orderBillDetails.set(index, orderBillDetail);
        calculateTotal();
    }

    public void delOrderBillDetail(OrderBillDetail orderBillDetail) {
        orderBillDetails.remove(orderBillDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        float allTotal = 0.0f;
        for (OrderBillDetail orderBillDetail : orderBillDetails) {
            allTotal += orderBillDetail.getNet();
        }
        this.total = allTotal;
    }

    public static OrderBill fromRS(ResultSet rs) {
        OrderBill orderBill = new OrderBill();
        System.out.println("getEmpId = "+orderBill.getEmpId());
        try {
            orderBill.setId(rs.getInt("order_id"));
            orderBill.setTotal(rs.getFloat("order_total"));
            orderBill.setOrderDate(rs.getTimestamp("order_date"));
            orderBill.setTax(rs.getFloat("order_tax"));
            orderBill.setVendorName(rs.getString("vendor_name"));
            orderBill.setEmpId(rs.getInt("emp_id"));

            //Population 
            EmployeeDao empDao = new EmployeeDao();
            System.out.println(orderBill);
            System.out.println("orderBill empId="+orderBill.getEmpId());
            Employee employee = empDao.get(orderBill.getEmpId());
            orderBill.setEmployee(employee);

        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return orderBill;
    }

}
