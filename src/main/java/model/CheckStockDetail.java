/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tauru
 */
public class CheckStockDetail {

    private int crdId;
    private String name;
    private int qty;
    private int NoEXP;
    private int NoDM;
    private float unitPrice;
    private float damageValue;
    private float expiryValue;
    private int checkId;
    private int rawmaterialId;

    public CheckStockDetail(int crdId, String name, int qty, int NoEXP, int NoDM, float unitPrice, float damageValue, float expiryValue, int checkId, int rawmaterialId) {
        this.crdId = crdId;
        this.name = name;
        this.qty = qty;
        this.NoEXP = NoEXP;
        this.NoDM = NoDM;
        this.unitPrice = unitPrice;
        this.damageValue = damageValue;
        this.expiryValue = expiryValue;
        this.checkId = checkId;
        this.rawmaterialId = rawmaterialId;
    }

    public CheckStockDetail(String name, int qty, int NoEXP, int NoDM, float unitPrice, float damageValue, float expiryValue, int checkId, int rawmaterialId) {
        this.crdId = -1;
        this.name = name;
        this.qty = qty;
        this.NoEXP = NoEXP;
        this.NoDM = NoDM;
        this.unitPrice = unitPrice;
        this.damageValue = damageValue;
        this.expiryValue = expiryValue;
        this.checkId = checkId;
        this.rawmaterialId = rawmaterialId;
    }

    public CheckStockDetail() {
        this.crdId = -1;
        this.name = "";
        this.qty = 0;
        this.NoEXP = 0;
        this.NoDM = 0;
        this.unitPrice = 0;
        this.damageValue = 0;
        this.expiryValue = 0;
        this.checkId = 0;
        this.rawmaterialId = 0;
    }

    public int getCrdId() {
        return crdId;
    }

    public void setCrdId(int crdId) {
        this.crdId = crdId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getNoEXP() {
        return NoEXP;
    }

    public void setNoEXP(int NoEXP) {
        this.NoEXP = NoEXP;
    }

    public int getNoDM() {
        return NoDM;
    }

    public void setNoDM(int NoDM) {
        this.NoDM = NoDM;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public float getDamageValue() {
        return damageValue;
    }

    public void setDamageValue(float damageValue) {
        this.damageValue = damageValue;
    }

    public float getExpiryValue() {
        return expiryValue;
    }

    public void setExpiryValue(float expiryValue) {
        this.expiryValue = expiryValue;
    }

    public int getCheckId() {
        return checkId;
    }

    public void setCheckId(int checkId) {
        this.checkId = checkId;
    }

    public int getRawmaterialId() {
        return rawmaterialId;
    }

    public void setRawmaterialId(int rawmaterialId) {
        this.rawmaterialId = rawmaterialId;
    }

    @Override
    public String toString() {
        return "CheckStockDetail{" + "crdId=" + crdId + ", name=" + name + ", qty=" + qty + ", NoEXP=" + NoEXP + ", NoDM=" + NoDM + ", unitPrice=" + unitPrice + ", damageValue=" + damageValue + ", expiryValue=" + expiryValue + ", checkId=" + checkId + ", rawmaterialId=" + rawmaterialId + '}';
    }

    public static CheckStockDetail fromRS(ResultSet rs) {
        CheckStockDetail checkStockDetail = new CheckStockDetail();
        try {
            checkStockDetail.setCrdId(rs.getInt("crd_id"));
            checkStockDetail.setName(rs.getString("rm_name"));
            checkStockDetail.setQty(rs.getInt("crd_qty"));
            checkStockDetail.setNoEXP(rs.getInt("crd_noexp"));
            checkStockDetail.setNoDM(rs.getInt("crd_nodm"));
            checkStockDetail.setUnitPrice(rs.getInt("crd_unit_price"));
            checkStockDetail.setDamageValue(rs.getInt("damage_value"));
            checkStockDetail.setExpiryValue(rs.getInt("exp_value"));
            checkStockDetail.setCheckId(rs.getInt("crm_id"));
            checkStockDetail.setRawmaterialId(rs.getInt("rm_id"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockDetail;
    }
}
