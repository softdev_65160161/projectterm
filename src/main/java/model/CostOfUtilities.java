/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author acer
 */
public class CostOfUtilities {

    private int id;
    private String status;
    private float totalPrice;
    private Date payDate;
    private int type;
    private int unit;
    private int empId;
    private Employee emp;

    public CostOfUtilities(int id, String status, float totalPrice, Date payDate, int type, int unit, int empId, Employee emp) {
        this.id = id;
        this.status = status;
        this.totalPrice = totalPrice;
        this.payDate = payDate;
        this.type = type;
        this.unit = unit;
        this.empId = empId;
    }

    public CostOfUtilities(String status, float totalPrice, Date payDate, int type, int unit, int empId) {
        this.id = -1;
        this.status = status;
        this.totalPrice = totalPrice;
        this.payDate = payDate;
        this.type = type;
        this.unit = unit;
        this.empId = empId;
    }

    public CostOfUtilities(String status, float totalPrice, int type, int unit, int empId) {
        this.id = -1;
        this.status = status;
        this.totalPrice = totalPrice;
        this.payDate = null;
        this.type = type;
        this.unit = unit;
        this.empId = empId;
    }

    public CostOfUtilities() {
        this.id = -1;
        this.status = "";
        this.totalPrice = 0;
        this.payDate = null;
        this.type = 0;
        this.unit = 0;
        this.empId = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public Employee getEmp() {
        return emp;
    }

    public void setEmp(Employee emp) {
        this.emp = emp;
        this.empId = emp.getId();
    }

    @Override
    public String toString() {
        return "CostOfUtilities{" + "id=" + id + ", status=" + status + ", totalPrice=" + totalPrice + ", payDate=" + payDate + ", type=" + type + ", unit=" + unit + ", empId=" + empId + ", emp=" + emp + '}';
    }

    public static CostOfUtilities fromRS(ResultSet rs) {
        CostOfUtilities costOfUtilities = new CostOfUtilities();
        try {
            costOfUtilities.setId(rs.getInt("cou_id"));
            costOfUtilities.setStatus(rs.getString("cou_status"));
            costOfUtilities.setTotalPrice(rs.getFloat("cou_total_price"));

//        // Parse the date from the database using a custom date format
//        String dateStr = rs.getString("cou_pay_date");
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = dateFormat.parse(dateStr);
//        // Set the formatted date as a Date object
            costOfUtilities.setPayDate(rs.getTimestamp("cou_pay_date"));
            costOfUtilities.setType(rs.getInt("cou_type"));
            costOfUtilities.setUnit(rs.getInt("cou_unit"));
            costOfUtilities.setEmpId(rs.getInt("emp_id"));

            // Population
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.get(costOfUtilities.getEmpId());
            costOfUtilities.setEmp(employee);

        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return costOfUtilities;
    }
}
