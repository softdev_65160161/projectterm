/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tauru
 */
public class CheckStock {

    private int checkId;
    private Date checkDate;
    private String checkStatus;
    private int empId;
    private ArrayList<CheckStockDetail> checkStockDetails = new ArrayList<CheckStockDetail>();
    private Employee employee;

    public CheckStock(int checkId, Date checkDate, String checkStatus, int empId) {
        this.checkId = checkId;
        this.checkDate = checkDate;
        this.checkStatus = checkStatus;
        this.empId = empId;
    }

    public CheckStock(int empId) {
        this.checkId = -1;
        this.checkDate = null;
        this.checkStatus = "";
        this.empId = empId;
    }

    public CheckStock() {
        this.checkId = -1;
        this.checkDate = null;
        this.checkStatus = "";
        this.empId = -1;
    }
    
    public CheckStock(Employee emp) {
        this.checkId = -1;
        this.checkDate = null;
        this.checkStatus = "";
        this.empId = emp.getId();
    }

    public int getCheckId() {
        return checkId;
    }

    public void setCheckId(int checkId) {
        this.checkId = checkId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.empId = employee.getId();
    }

    public ArrayList<CheckStockDetail> getCheckStockDetails() {
        return checkStockDetails;
    }

    public void setCheckStockDetails(ArrayList<CheckStockDetail> checkStockDetails) {
        this.checkStockDetails = checkStockDetails;
    }
    

    public void addCheckStockDetail(CheckStockDetail checkStockDetail) {
        checkStockDetails.add(checkStockDetail);
    }

    public void addCheckStockDetail(RawMaterial rawMaterial, int qty, int NoEXP, int NoDm) {
        CheckStockDetail cd = new CheckStockDetail(rawMaterial.getRawmaterialName(), qty, NoEXP, NoDm, rawMaterial.getUnitPrice(), rawMaterial.getUnitPrice() * NoDm, rawMaterial.getUnitPrice() * NoEXP, -1, rawMaterial.getRawmaterialId());
        checkStockDetails.add(cd);
    }
    
    public void addCheckStockDetail(RawMaterial rawMaterial, int qty) {
        CheckStockDetail cd = new CheckStockDetail(rawMaterial.getRawmaterialName(), qty, 0, 0, rawMaterial.getUnitPrice(), rawMaterial.getUnitPrice() * 0, rawMaterial.getUnitPrice() * 0, -1, rawMaterial.getRawmaterialId());
        checkStockDetails.add(cd);
    }

    public void delCheckStockDetail(CheckStockDetail checkStockDetail) {
        checkStockDetails.remove(checkStockDetail);
    }

    @Override
    public String toString() {
        return "CheckStock{" + "checkId=" + checkId + ", checkDate=" + checkDate + ", checkStatus=" + checkStatus + ", empId=" + empId + ", checkStockDetails=" + checkStockDetails + ", employee=" + employee + '}';
    }

    

    public static CheckStock fromRS(ResultSet rs) {
        CheckStock checkStock = new CheckStock();
        try {
            checkStock.setCheckId(rs.getInt("crm_id"));
            checkStock.setCheckDate(rs.getTimestamp("crm_date"));
            checkStock.setCheckStatus(rs.getString("crm_status"));
            checkStock.setEmpId(rs.getInt("emp_id"));

            //Population
            EmployeeDao employeeDao = new EmployeeDao();
            System.out.println(checkStock);
            System.out.println("checkStock empid ="+checkStock.getEmpId());
            Employee employee = employeeDao.get(checkStock.getEmpId());
            checkStock.setEmployee(employee);

        } catch (SQLException ex) {
            Logger.getLogger(CheckStock.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStock;
    }
}
