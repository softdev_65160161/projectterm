/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class Vendor {
    private int id;
    private String name; 
    private String contract;
    private String phone;

    public Vendor(int id, String name, String contract, String phone) {
        this.id = id;
        this.name = name;
        this.contract = contract;
        this.phone = phone;
    }
    
    public Vendor(String name, String contract, String phone) {
        this.id = -1;
        this.name = name;
        this.contract = contract;
        this.phone = phone;
    }
    
    public Vendor() {
        this.id = -1;
        this.name = "";
        this.contract = "";
        this.phone = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContract() {
        return contract;
    }

    public void setContract(String contract) {
        this.contract = contract;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Vendor{" + "id=" + id + ", name=" + name + ", contract=" + contract + ", phone=" + phone + '}';
    }
    
    public static Vendor fromRS(ResultSet rs){
        Vendor vendor = new Vendor();
        try {
            vendor.setId(rs.getInt("vendor_id"));
            vendor.setName(rs.getString("vendor_name"));
            vendor.setContract(rs.getString("vendor_contract"));
            vendor.setPhone(rs.getString("vendor_phone"));
        } catch (SQLException ex) {
            Logger.getLogger(Vendor.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return vendor;
    }
   
}
