/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class OrderBillDetail {

    private int id;
    private float unitPrice;
    private int qty;
    private float net;
    private int rawMaterialId;
    private int orderId;

    public OrderBillDetail(int id, float unitPrice, int qty,float net, int rawMaterialId, int orderId) {
        this.id = id;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.net = net;
        this.rawMaterialId = rawMaterialId;
        this.orderId = orderId;
    }

    public OrderBillDetail(float unitPrice, int qty, float net, int rawMaterialId, int orderId) {
        this.id = -1;
        this.unitPrice = unitPrice;
        this.qty = qty;
        this.net = net;
        this.rawMaterialId = rawMaterialId;
        this.orderId = orderId;
    }

    public OrderBillDetail() {
        this.id = -1;
        this.unitPrice = 0;
        this.qty = 0;
        this.net = 0;
        this.rawMaterialId = -1;
        this.orderId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    
    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public float getNet() {
        return net;
    }

    public void setNet(float net) {
        this.net = net;
    }

    public int getRawMaterialId() {
        return rawMaterialId;
    }

    public void setRawMaterialId(int rawMaterialId) {
        this.rawMaterialId = rawMaterialId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderBillDetail{" + "id=" + id + ", unitPrice=" + unitPrice + ", qty=" + qty + ", net=" + net + ", rawMaterialId=" + rawMaterialId + ", orderId=" + orderId + '}';
    }

    public static OrderBillDetail fromRS(ResultSet rs) {
        OrderBillDetail orderBillDetail = new OrderBillDetail();
        try {

            orderBillDetail.setId(rs.getInt("orbd_id"));
            orderBillDetail.setUnitPrice(rs.getFloat("orbd_unit_price"));
            orderBillDetail.setQty(rs.getInt("orbd_qty"));
            orderBillDetail.setNet(rs.getInt("orbd_net"));
            orderBillDetail.setRawMaterialId(rs.getInt("rm_id"));
            orderBillDetail.setOrderId(rs.getInt("order_id"));
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return orderBillDetail;
    }
}
