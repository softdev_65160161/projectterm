 /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import dao.EmployeeDao;
import dao.MemberDao;
import dao.RecieptDetailDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nutta
 */
public class Reciept {

    private int id;
    private Date createdDate;
    private float total;
    private float cash;
    private int totalQty;
    private int empId;
    private int memId;
    private Employee employee;
    private Member member;

    private ArrayList<RecieptDetail> recieptDetails = new ArrayList();

    public Reciept(int id, Date createdDate, float total, float cash, int totalQty, int empId, int memId) {
        this.id = id;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.empId = empId;
        this.memId = memId;

    }

    public Reciept(Date createdDate, float total, float cash, int totalQty, int empId, int memId) {
        this.id = -1;
        this.createdDate = createdDate;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.empId = empId;
        this.memId = memId;

    }

    public Reciept(float total, float cash, int totalQty, int empId, int memId) {
        this.id = -1;
        this.createdDate = null;
        this.total = total;
        this.cash = cash;
        this.totalQty = totalQty;
        this.empId = empId;
        this.memId = memId;

    }

    public Reciept(float cash, int empId, int memId) {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = cash;
        this.totalQty = 0;
        this.empId = empId;
        this.memId = memId;

    }

    public Reciept() {
        this.id = -1;
        this.createdDate = null;
        this.total = 0;
        this.cash = 0;
        this.totalQty = 0;
        this.empId = 0;
        this.memId = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public float getCash() {
        return cash;
    }

    public void setCash(float cash) {
        this.cash = cash;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public int getMemId() {
        return memId;
    }

    public void setMemId(int memId) {
        this.memId = memId;
    }

    public ArrayList<RecieptDetail> getRecieptDetails() {
        return recieptDetails;
    }

    public void setRecieptDetails(ArrayList<RecieptDetail> recieptDetails) {
        this.recieptDetails = recieptDetails;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.empId = employee.getId();
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
        this.memId = member.getId();
    }

    @Override
    public String toString() {
        return "Reciept{" + "id=" + id + ", createdDate=" + createdDate + ", total=" + total + ", cash=" + cash + ", totalQty=" + totalQty + ", empId=" + empId + ", memId=" + memId + ", employee=" + employee + ", member=" + member + ", recieptDetails=" + recieptDetails + '}';
    }

    

    public void addRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.add(recieptDetail);
        calculateTotal();
    }

    public void addRecieptDetail(Product product, int qty) {

        if (checkName(product) != -1) {
            RecieptDetail rd = recieptDetails.get(checkName(product));
            int Qty = rd.getQty();
            rd.setQty(Qty + qty);
        } else {
            RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(),
                    product.getPrice(), qty, qty * product.getPrice(), -1);
            recieptDetails.add(rd);
        }
        calculateTotal();
    }

    public void addRecieptDetail(Product product, int qty, String type, String sweet) {
        if (check(product, type, sweet)) {
            RecieptDetail rd = recieptDetails.get(checkSweet(sweet, type));
            int Qty = rd.getQty();
            rd.setQty(Qty + qty);
        } else {
            RecieptDetail rd = new RecieptDetail(product.getId(), product.getName(),
                    product.getPrice(), qty, qty * product.getPrice(), -1, type, sweet);

            recieptDetails.add(rd);
        }
        calculateTotal();
    }

    public int checkName(Product product) {
        for (int i = 0; i < recieptDetails.size(); i++) {
            RecieptDetail rd = recieptDetails.get(i);
            if (rd.getProductId() == product.getId()) {
                return i;
            }
        }
        return -1;
    }

    private int checkType(String type) {
        for (int i = 0; i < recieptDetails.size(); i++) {
            RecieptDetail rd = recieptDetails.get(i);
            if (rd.getProductType().equals(type)) {
                return i;
            }
        }
        return -1;
    }

    private int checkSweet(String sweet, String type) {
        for (int i = 0; i < recieptDetails.size(); i++) {
            RecieptDetail rd = recieptDetails.get(i);
            if (rd.getProductSweet().equals(sweet) && rd.getProductType().equals(type)) {
                return i;
            }
        }
        return -1;
    }

    private boolean check(Product product, String type, String sweet) {
        if (checkName(product) == -1) {
            return false;
        }
        return checkSweet(sweet, type) != -1;
    }

    public void delRecieptDetail(RecieptDetail recieptDetail) {
        recieptDetails.remove(recieptDetail);
        calculateTotal();
    }

    public void calculateTotal() {
        int totalQty = 0;
        float total = 0.0f;
        for (RecieptDetail rd : recieptDetails) {
            total += rd.getTotalPrice();
            totalQty += rd.getQty();
        }

        this.totalQty = totalQty;
        this.total = total;
    }

    public static Reciept fromRS(ResultSet rs) {
        Reciept reciept = new Reciept();
        try {
            reciept.setId(rs.getInt("reciept_id"));
            reciept.setCreatedDate(rs.getTimestamp("created_date"));
            reciept.setTotal(rs.getFloat("total"));
            reciept.setCash(rs.getFloat("cash"));
            reciept.setTotalQty(rs.getInt("total_qty"));
            reciept.setEmpId(rs.getInt("emp_id"));
            reciept.setMemId(rs.getInt("mem_id"));

            //Population
            MemberDao memberDao = new MemberDao();
            EmployeeDao employeeDao = new EmployeeDao();
            Member member = memberDao.get(reciept.getMemId());
            Employee employee = employeeDao.get(reciept.getEmpId());
            reciept.setMember(member);
            reciept.setEmployee(employee);

        } catch (SQLException ex) {
            Logger.getLogger(Reciept.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return reciept;
    }

}
