/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package uiProduct;

import component.BuyProductable;
import component.ProductListPanel;
import dao.RecieptDetailDao;
import java.awt.Component;
import java.awt.Image;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.nio.file.Files.list;
import static java.rmi.Naming.list;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.Date;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.AbstractTableModel;
import model.Employee;
import model.Member;
import model.Product;
import model.Reciept;
import model.RecieptDetail;
import service.EmployeeService;
import service.MemberService;
import service.ProductService;
import service.RecieptService;
import ui.MemberDialog;

/**
 *
 * @author nutta
 */
public class PosPanel extends javax.swing.JPanel implements BuyProductable {

    ArrayList<Product> products;
    ProductService productService = new ProductService();
    RecieptService recieptService = new RecieptService();
    Reciept reciept;
    private final ProductListPanel productListPanel;
    private Component rootPane;
    EmployeeService employeeService = new EmployeeService();
    MemberService memberService = new MemberService();
    private RecieptDetailDao editedTable;
    private List<Reciept> list;

    /**
     * Creates new form posPanel
     */
    public PosPanel() {
        initComponents();
        reciept = new Reciept();
        reciept.setEmployee(EmployeeService.getCurrentUser());

        tblRecieptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"Name", "Price", "Qty", "Type", "Sweet", "TotalPrice"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                if (reciept != null && reciept.getRecieptDetails() != null) {
                    return reciept.getRecieptDetails().size();
                } else {
                    return 0;
                }
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                if (reciept != null && reciept.getRecieptDetails() != null) {
                    ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                    if (rowIndex < recieptDetails.size()) {
                        RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                        switch (columnIndex) {
                            case 0:
                                return recieptDetail.getProductName();
                            case 1:
                                return recieptDetail.getProductPrice();
                            case 2:
                                return recieptDetail.getQty();
                            case 3:
                                return recieptDetail.getProductType();
                            case 4:
                                return recieptDetail.getProductSweet();
                            case 5:
                                return recieptDetail.getTotalPrice();
                            default:
                                throw new AssertionError();
                        }
                    }
                }
                return null;
            }

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                if (reciept != null && reciept.getRecieptDetails() != null) {
                    ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
                    if (rowIndex < recieptDetails.size()) {
                        RecieptDetail recieptDetail = recieptDetails.get(rowIndex);
                        if (columnIndex == 2) {
                            try {
                                int qty = Integer.parseInt((String) aValue);
                                if (qty < 1) {
                                    return;
                                }
                                recieptDetail.setQty(qty);
                                reciept.calculateTotal();
                                refreshReciept();
                            } catch (NumberFormatException e) {
                                // Handle the NumberFormatException appropriately
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return columnIndex == 2 && reciept != null && reciept.getRecieptDetails() != null && rowIndex < reciept.getRecieptDetails().size();
            }
        });

        productListPanel = new ProductListPanel();
        productListPanel.addOnBuyProduct(this);
        scrProductList.setViewportView(productListPanel);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        scrProductList = new javax.swing.JScrollPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblRecieptDetail = new javax.swing.JTable();
        btnConfirm = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        btnRegisterMember = new javax.swing.JButton();
        btnMemberSearch = new javax.swing.JButton();
        btnPromotion = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        lblSubTotal = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        lblMemberName = new javax.swing.JLabel();
        lblPoint = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        lblCash = new javax.swing.JLabel();
        cashTextField = new javax.swing.JTextField();
        btnPromptPay = new javax.swing.JButton();
        btnCalculate = new javax.swing.JButton();

        jPanel2.setBackground(new java.awt.Color(231, 208, 185));

        scrProductList.setBackground(new java.awt.Color(169, 163, 37));
        scrProductList.setForeground(new java.awt.Color(255, 255, 255));
        scrProductList.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        tblRecieptDetail.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        tblRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tblRecieptDetail);

        btnConfirm.setBackground(new java.awt.Color(153, 255, 153));
        btnConfirm.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnConfirm.setForeground(new java.awt.Color(0, 0, 0));
        btnConfirm.setText("Confirm");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        btnCancel.setBackground(new java.awt.Color(255, 102, 102));
        btnCancel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(0, 0, 0));
        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        btnRegisterMember.setBackground(new java.awt.Color(201, 136, 96));
        btnRegisterMember.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnRegisterMember.setForeground(new java.awt.Color(0, 0, 0));
        btnRegisterMember.setText("Register Member");
        btnRegisterMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegisterMemberActionPerformed(evt);
            }
        });

        btnMemberSearch.setBackground(new java.awt.Color(201, 136, 96));
        btnMemberSearch.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnMemberSearch.setForeground(new java.awt.Color(0, 0, 0));
        btnMemberSearch.setText("Member Search");
        btnMemberSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMemberSearchActionPerformed(evt);
            }
        });

        btnPromotion.setBackground(new java.awt.Color(201, 136, 96));
        btnPromotion.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPromotion.setForeground(new java.awt.Color(0, 0, 0));
        btnPromotion.setText("Promotion");
        btnPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromotionActionPerformed(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(231, 208, 185));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 2));

        lblSubTotal.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblSubTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblSubTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblSubTotal.setText("Subtotal : 0.0");

        jPanel5.setBackground(new java.awt.Color(188, 155, 107));

        lblMemberName.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblMemberName.setForeground(new java.awt.Color(0, 0, 0));
        lblMemberName.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblMemberName.setText("Member Name : -");

        lblPoint.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblPoint.setForeground(new java.awt.Color(0, 0, 0));
        lblPoint.setText("Member Point : -");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblPoint, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblMemberName)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(lblPoint)
                .addContainerGap())
        );

        lblDiscount.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblDiscount.setForeground(new java.awt.Color(0, 0, 0));
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDiscount.setText("Discount : 0.0");

        lblTotal.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblTotal.setForeground(new java.awt.Color(0, 0, 0));
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblTotal.setText("Total :  0.0  ");

        lblChange.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblChange.setForeground(new java.awt.Color(0, 0, 0));
        lblChange.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblChange.setText("Change : 0.0");

        lblCash.setFont(new java.awt.Font("Leelawadee UI", 0, 18)); // NOI18N
        lblCash.setForeground(new java.awt.Color(0, 0, 0));
        lblCash.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblCash.setText("Cash: ");

        cashTextField.setBackground(new java.awt.Color(255, 255, 255));
        cashTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cashTextFieldActionPerformed(evt);
            }
        });

        btnPromptPay.setBackground(new java.awt.Color(201, 136, 96));
        btnPromptPay.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPromptPay.setForeground(new java.awt.Color(0, 0, 0));
        btnPromptPay.setText("PromptPay ");
        btnPromptPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPromptPayActionPerformed(evt);
            }
        });

        btnCalculate.setBackground(new java.awt.Color(201, 136, 96));
        btnCalculate.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnCalculate.setForeground(new java.awt.Color(0, 0, 0));
        btnCalculate.setText("Calculate");
        btnCalculate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(lblSubTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblCash)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cashTextField))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblChange, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(btnCalculate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPromptPay)))
                        .addGap(0, 4, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblSubTotal)
                    .addComponent(lblCash)
                    .addComponent(cashTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblDiscount)
                    .addComponent(lblChange))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotal)
                    .addComponent(btnPromptPay)
                    .addComponent(btnCalculate))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 458, Short.MAX_VALUE)
                        .addComponent(btnMemberSearch)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRegisterMember)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPromotion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(scrProductList)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 462, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(scrProductList, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegisterMember, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMemberSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnConfirm, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1073, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 541, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        String cash = (cashTextField.getText());
        if (cash.equals("") || cash.equals("0.0")) {
            JOptionPane.showMessageDialog(this, "Plaese Input Cash in Text Field", "Warning", JOptionPane.WARNING_MESSAGE);
        } else {
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, 1);
            if (input == 0) {
                MemberService memService = new MemberService();
                reciept.setCash(Float.parseFloat(cashTextField.getText()));
                recieptService.addNew(reciept);
                System.out.println("" + reciept);
                openRecieptDialog();
                clearReciept();
                cashTextField.setText("");
                Member memcurr = memService.currentMember = new Member();
                System.out.println(memcurr);
                System.out.println("Add into RecieptDetail");
            } else {
                System.out.println("Cancel");
            }
        }

    }//GEN-LAST:event_btnConfirmActionPerformed

    public void openRecieptDialog() {
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        RecieptDialog rd = new RecieptDialog(frame, reciept);
        rd.setLocationRelativeTo(this);
        rd.setVisible(true);
    }

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearReciept();
        cashTextField.setText("");
        System.out.println("Clear Table");
        MemberService memService = new MemberService();
        Member memcurr = memService.currentMember = new Member();
        System.out.println(memcurr);
    }//GEN-LAST:event_btnCancelActionPerformed

    private void btnMemberSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMemberSearchActionPerformed

        openSearchMemDialog();
        reciept.setMember(MemberService.getCurrentMember());
        System.out.println("" + MemberService.getCurrentMember());
        lblMemberName.setText("Member Name : " + memberService.currentMember.getName());
        lblPoint.setText("Member Point : " + memberService.currentMember.getPoint());
    }//GEN-LAST:event_btnMemberSearchActionPerformed

    private void btnPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromotionActionPerformed

    }//GEN-LAST:event_btnPromotionActionPerformed

    private void cashTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cashTextFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cashTextFieldActionPerformed

    private void btnPromptPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPromptPayActionPerformed
        cashTextField.setText("" + reciept.getTotal());
        lblChange.setText("Change : 0.0");
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        QrcodeDialog qrDialog = new QrcodeDialog(frame, true);
        qrDialog.setLocationRelativeTo(this);
        qrDialog.setVisible(true);

    }//GEN-LAST:event_btnPromptPayActionPerformed

    private void btnCalculateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculateActionPerformed
        try {
            float total = reciept.getTotal();
            float cash = Float.parseFloat(cashTextField.getText());
            if (cash >= total) {
                float change = cash - total;
                lblChange.setText("Change : " + change);
            } else {
                lblChange.setText("Change : 0.0");
                JOptionPane.showMessageDialog(this,"Cash is Lower than total", "Warning",JOptionPane.INFORMATION_MESSAGE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }//GEN-LAST:event_btnCalculateActionPerformed

    private void btnRegisterMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegisterMemberActionPerformed
        Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        MemberDialog userDialog = new MemberDialog(frame, editedMember);
        userDialog.setLocationRelativeTo(this);
        userDialog.setVisible(true);
    }//GEN-LAST:event_btnRegisterMemberActionPerformed

    private void clearReciept() {
        reciept = new Reciept();
//        reciept.setMember(MemberService.getCurrentMember());
        reciept.setEmployee(EmployeeService.getCurrentUser());
        refreshReciept();
    }

    public void openSearchMemDialog() {
        Member editedMember = new Member();
        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        SearchMemDialog userDialog = new SearchMemDialog(frame, editedMember);
        userDialog.setLocationRelativeTo(this);
        userDialog.setVisible(true);
    }

    private void refreshReciept() {
        tblRecieptDetail.revalidate();
        tblRecieptDetail.repaint();
        lblSubTotal.setText("Subtotal : " + reciept.getTotal());
//        lblDiscount.setText("Discount: ");
        lblTotal.setText("Total : " + reciept.getTotal());
        lblMemberName.setText("Member Name : - ");
        lblPoint.setText("Member Point : -");
        lblChange.setText("Change : 0.0");

        ArrayList<RecieptDetail> recieptDetails = reciept.getRecieptDetails();
        for (RecieptDetail r : recieptDetails) {
            System.out.println(r);
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalculate;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton btnMemberSearch;
    private javax.swing.JButton btnPromotion;
    private javax.swing.JButton btnPromptPay;
    private javax.swing.JButton btnRegisterMember;
    private javax.swing.JTextField cashTextField;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblCash;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblMemberName;
    private javax.swing.JLabel lblPoint;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JScrollPane scrProductList;
    private javax.swing.JTable tblRecieptDetail;
    // End of variables declaration//GEN-END:variables
@Override
    public void buy(Product product, int qty) {
        reciept.addRecieptDetail(product, qty);
        refreshReciept();
    }

    @Override
    public void buy(Product product, int qty, String type, String sweet) {
        reciept.addRecieptDetail(product, qty, type, sweet);
        refreshReciept();
    }
}
