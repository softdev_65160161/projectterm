/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.OrderBillDao;
import dao.OrderBillDetailDao;
import java.util.List;
import model.OrderBill;
import model.OrderBillDetail;



/**
 *
 * @author nutta
 */
public class OrderBillService {

    public OrderBill getById(int id) {
        OrderBillDao orderBillDao = new OrderBillDao();
        return orderBillDao.get(id);
    }

    public List<OrderBill> getOrderBills() {
        OrderBillDao orderBillDao = new OrderBillDao();
        return orderBillDao.getAll();
    }

    public OrderBill addNew(OrderBill editedOrderBill) {
        OrderBillDao orderBillDao = new OrderBillDao();
        OrderBillDetailDao orderBillDetailDao = new OrderBillDetailDao();
        OrderBill orderBill = orderBillDao.save(editedOrderBill);
        for(OrderBillDetail od: editedOrderBill.getOrderBillDetial()){
            od.setOrderId(orderBill.getId());
            orderBillDetailDao.save(od);
        }
        return orderBill;
    }

    public OrderBill update(OrderBill editedOrderBill) {
        OrderBillDao orderBillDao = new OrderBillDao();
        return orderBillDao.update(editedOrderBill);
    }

    public int delete(OrderBill editedOrderBill) {
        OrderBillDao orderBillDao = new OrderBillDao();
        return orderBillDao.delete(editedOrderBill);
    }
}
