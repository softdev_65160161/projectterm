/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.TimeAttendanceDao;
import java.util.List;
import model.TimeAttendance;

/**
 *
 * @author werapan
 */
public class TimeAttendanceService {

    public TimeAttendance getById(int id) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.get(id);
    }

    public List<TimeAttendance> getTimeAttendances() {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.getAll(" ta_date, emp_id, ta_type ASC");
    }

    public List<TimeAttendance> getTimeAttendanceByIdAndType(int id, String type) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.getAll("WHERE emp_id = " + id + " AND ta_type = '" + type + "'", "ORDER BY ta_date ASC");
    }

    public List<TimeAttendance> getTimeAttendanceById(int id) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.getAll("WHERE emp_id = " + id, "ORDER BY ta_date ASC");
    }

    public List<TimeAttendance> getTimeAttendanceByDay(String bebin, String end) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.getAll(bebin, end, "ORDER BY ta_date ASC");
    }

    public List<TimeAttendance> getTimeAttendanceByStatus(String status) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.getAll("WHERE ta_status = '" + status + "'", "ORDER BY ta_date, emp_id, ta_type ASC");
    }

    public TimeAttendance addNew(TimeAttendance editedTimeAttendance) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();

        timeAttendanceDao.save(editedTimeAttendance);

        return editedTimeAttendance;
    }

    public TimeAttendance update(TimeAttendance editedTimeAttendance) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.update(editedTimeAttendance);
    }

    public int delete(TimeAttendance editedTimeAttendance) {
        TimeAttendanceDao timeAttendanceDao = new TimeAttendanceDao();
        return timeAttendanceDao.delete(editedTimeAttendance);
    }

}
