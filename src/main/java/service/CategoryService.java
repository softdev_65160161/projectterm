/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CategoryDao;

import model.Category;

import java.util.List;

/**
 *
 * @author werapan
 */
public class CategoryService {

    public Category getById(int id) {
        CategoryDao categoryDao = new  CategoryDao();
        return categoryDao.get(id);
    }

    public List<Category> getCategorys() {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.getAll(" category_id ASC");
    }

    public Category addNew(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.save(editedCategory);
    }

    public Category update(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.update(editedCategory);
    }

    public int delete(Category editedCategory) {
        CategoryDao categoryDao = new CategoryDao();
        return categoryDao.delete(editedCategory);
    }

    public Object getCategoryDeatails() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
