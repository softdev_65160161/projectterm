/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.EmployeeDao;
import java.util.List;
import model.Employee;

/**
 *
 * @author werapan
 */
public class EmployeeService {

    public static Employee currentUser;

    public Employee login(String login, String password) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.getByLogin(login);
        if (employee != null && employee.getPassword().equals(password)) {
            currentUser = employee;
            return employee;
        }
        return null;
    }

    public static Employee getCurrentUser(){
        return currentUser;
    }
    public Employee getById(int id) {
        EmployeeDao employeeDao = new EmployeeDao();
        Employee employee = employeeDao.get(id);
        return employee;
    }
    
    public List<Employee> getEmployees() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll(" emp_login asc");
    }

    public Employee addNew(Employee editedEmployee)throws ValidateException {
        if(!editedEmployee.isValid()){
              throw new ValidateException("Employee is invalid");
         }
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) throws ValidateException {
         if(!editedEmployee.isValid()){
              throw new ValidateException("Employee is invalid");
         }
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }
}
