/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CostOfUtilitiesDao;
import java.util.List;
import model.CostOfUtilities;

/**
 *
 * @author acer
 */
public class CostOfUtilitiesService {
    public CostOfUtilities getById(int id) {
        CostOfUtilitiesDao costOfUtilitiesDao = new  CostOfUtilitiesDao();
        return costOfUtilitiesDao.get(id);
    }

    public List<CostOfUtilities> getCostOfUtilitiess() {
        CostOfUtilitiesDao costOfUtilitiesDao = new CostOfUtilitiesDao();
        return costOfUtilitiesDao.getAll(" cou_id ASC");
    }

    public CostOfUtilities addNew(CostOfUtilities editedCostOfUtilities) {
        CostOfUtilitiesDao costOfUtilitiesDao = new CostOfUtilitiesDao();
        return costOfUtilitiesDao.save(editedCostOfUtilities);
    }

    public CostOfUtilities update(CostOfUtilities editedCostOfUtilities) {
        CostOfUtilitiesDao costOfUtilitiesDao = new CostOfUtilitiesDao();
        return costOfUtilitiesDao.update(editedCostOfUtilities);
    }

    public int delete(CostOfUtilities editedCostOfUtilities) {
        CostOfUtilitiesDao costOfUtilitiesDao = new CostOfUtilitiesDao();
        return costOfUtilitiesDao.delete(editedCostOfUtilities);
    }

    
}

