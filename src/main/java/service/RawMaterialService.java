/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.RawmaterialDao;
import model.RawMaterial;
import java.util.ArrayList;

/**
 *
 * @author 65160
 */
public class RawMaterialService {

    public ArrayList<RawMaterial> getLowItem() {
        ArrayList<RawMaterial> materials;
        RawmaterialDao materialDao = new RawmaterialDao();
        materials = (ArrayList<RawMaterial>) materialDao.getAll("rm_qoh < rm_minimum", "rm_qoh asc");

        return materials;
    }
    
    public RawMaterial getById(int id) {
        RawMaterial rawMaterials;
        RawmaterialDao materialDao = new RawmaterialDao();
        rawMaterials = materialDao.get(id);

        return rawMaterials;
    }
    
    public RawMaterial getByName(String name){
        RawMaterial material;
        RawmaterialDao materialDao = new RawmaterialDao();
        material = materialDao.getByName(name);
        
        return material;
    }

    public ArrayList<RawMaterial> getAll() {
        ArrayList<RawMaterial> materials;
        RawmaterialDao materialDao = new RawmaterialDao();
        materials = (ArrayList<RawMaterial>) materialDao.getAll("rm_id");

        return materials;
    }
    
    
    public void addNew(RawMaterial material)throws ValidateException{
        if(!material.isValid()){
            throw new ValidateException("Raw Material is invalid!!!");
        }
        RawmaterialDao materialDao = new RawmaterialDao();
        materialDao.save(material);
    }
    
    public void update(RawMaterial material)throws ValidateException{
        if(!material.isValid()){
            throw new ValidateException("Raw Material is invalid!!!");
        }
        RawmaterialDao materialDao = new RawmaterialDao();
        materialDao.update(material);
    }
    
    public void delete(RawMaterial material){
        RawmaterialDao materialDao = new RawmaterialDao();
        materialDao.delete(material);
    }
}
