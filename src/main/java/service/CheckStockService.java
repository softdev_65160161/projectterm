/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.CheckStockDao;
import dao.CheckStockDetailDao;
import model.CheckStock;
import model.CheckStockDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CheckStockService {

    public CheckStock getById(int id) {
        CheckStockDao checkStockDao = new  CheckStockDao();
        return checkStockDao.get(id);
    }

    public List<CheckStock> getCheckStocks() {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.getAll(" crm_id ASC");
    }

    public CheckStock addNew(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        CheckStock checkStock = checkStockDao.save(editedCheckStock);
        
        System.out.println("in addNew");
        System.out.println(checkStock);
        System.out.println("----------------");
        
        for(CheckStockDetail rd: editedCheckStock.getCheckStockDetails()){
            rd.setCheckId(checkStock.getCheckId());
            checkStockDetailDao.save(rd);
        }
        return checkStock;
    }

    public CheckStock update(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.update(editedCheckStock);
    }

    public int delete(CheckStock editedCheckStock) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.delete(editedCheckStock);
    }

}
