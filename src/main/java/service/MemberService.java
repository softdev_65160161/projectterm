/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;

import dao.MemberDao;
import model.Member;
import java.util.List;

/**
 *
 * @author nutta
 */
public class MemberService {
    
    public static Member currentMember;
    
    public Member getById(int id) {
        MemberDao memberDao = new MemberDao();
        return memberDao.get(id);
    }
    
    public Member getPoint(int point) {
        MemberDao memberDao = new MemberDao();
        return memberDao.get(point);
    }
    
    public Member getByTel(String tel) {
        MemberDao memberDao = new MemberDao();
        Member member = memberDao.getByTel(tel);
        if (member != null) {
            currentMember = member;
            return member;
        }
        return null;
    }
    
    public static Member getCurrentMember(){
        return currentMember;
    }
    
    public List<Member> getMembers(){
        MemberDao memberDao = new MemberDao();
        return memberDao.getAll(" mem_id ASC");
    }

    public Member addNew(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.update(editedMember);
    }

    public int delete(Member editedMember) {
        MemberDao memberDao = new MemberDao();
        return memberDao.delete(editedMember);
    }
}
