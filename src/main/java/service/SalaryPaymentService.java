/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package service;
import dao.SalaryPaymentDao;
import java.util.List;
import model.SalaryPayment;

/**
 *
 * @author MSiRTX
 */
public class SalaryPaymentService {
    public SalaryPayment getById(int id) {
        SalaryPaymentDao SalaryDao = new SalaryPaymentDao();
        SalaryPayment Salary = SalaryDao.get(id);
        return Salary;
    }
    
    public List<SalaryPayment> getSalaryPayments(){
        SalaryPaymentDao SalaryDao = new SalaryPaymentDao();
        return SalaryDao.getAll(" sp_id ASC");
    }

    public SalaryPayment addNew(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao SalaryDao = new SalaryPaymentDao();
        return SalaryDao.save(editedSalaryPayment);
    }

    public SalaryPayment update(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao SalaryDao = new SalaryPaymentDao();
        return SalaryDao.update(editedSalaryPayment);
    }

    public int delete(SalaryPayment editedSalaryPayment) {
        SalaryPaymentDao SalaryDao = new SalaryPaymentDao();
        return SalaryDao.delete(editedSalaryPayment);
    }
}
