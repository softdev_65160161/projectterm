/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import model.CheckStockDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class CheckStockDetailDao implements Dao<CheckStockDetail> {

    @Override
    public CheckStockDetail get(int id) {
        CheckStockDetail checkStockDetail = null;
        String sql = "SELECT * FROM checkstock_detail WHERE crd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStockDetail = CheckStockDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockDetail;
    }

    @Override
    public List<CheckStockDetail> getAll() {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkstock_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CheckStockDetail> getAll(String where, String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkstock_detail WHERE " + where + " ORDER BY " + order;

        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckStockDetail> getAll(String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM checkstock_detail ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStockDetail save(CheckStockDetail obj) {
        String sql = "INSERT INTO checkstock_detail (rm_name,crd_qty, crd_noexp, crd_nodm, crd_unit_price, damage_value,exp_value, crm_id, rm_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setInt(2, obj.getQty());
            stmt.setInt(3, obj.getNoEXP());
            stmt.setInt(4, obj.getNoDM());
            stmt.setFloat(5, obj.getUnitPrice());
            stmt.setFloat(6, obj.getDamageValue());
            stmt.setFloat(7, obj.getExpiryValue());
            stmt.setInt(8, obj.getCheckId());
            stmt.setInt(9, obj.getRawmaterialId());
                 //            System.out.println(stmt);
                stmt.executeUpdate();
                int id = DatabaseHelper.getInsertedId(stmt);
                obj.setCrdId(id);
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
            return obj;
        }

        @Override
        public CheckStockDetail update(CheckStockDetail obj) {
        String sql = "UPDATE checkstock_detail"
                    + " SET rm_name = ?, crd_qty = ?, crd_noexp = ?, crd_nodm = ?, crd_unit_price = ?, damage_value = ?, exp_value = ?, crm_id = ?, rm_id = ?"
                    + " WHERE crd_id = ?";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);

                stmt.setString(1, obj.getName());
                stmt.setInt(2, obj.getQty());
                stmt.setInt(3, obj.getNoEXP());
                stmt.setInt(4, obj.getNoDM());
                stmt.setFloat(5, obj.getUnitPrice());
                stmt.setFloat(6, obj.getDamageValue());
                stmt.setFloat(7, obj.getExpiryValue());
                stmt.setInt(8, obj.getCheckId());
                stmt.setInt(9, obj.getRawmaterialId());
                stmt.setInt(10, obj.getCrdId());
//            System.out.println(stmt);
                int ret = stmt.executeUpdate();
                System.out.println(ret);
                return obj;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                return null;
            }
        }

        @Override
        public int delete(CheckStockDetail obj) {
        String sql = "DELETE FROM checkstock_detail WHERE crd_id=?";
            Connection conn = DatabaseHelper.getConnect();
            try {
                PreparedStatement stmt = conn.prepareStatement(sql);
                stmt.setInt(1, obj.getCrdId());
                int ret = stmt.executeUpdate();
                return ret;
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
            return -1;
        }

    }
