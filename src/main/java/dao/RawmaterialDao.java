/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import model.RawMaterial;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tauru
 */
public class RawmaterialDao implements Dao<RawMaterial>{

    @Override
    public RawMaterial get(int id) {
        RawMaterial rawmaterial = null;
        String sql = "SELECT * FROM rawmaterial WHERE rm_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                rawmaterial = RawMaterial.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return rawmaterial;
    }

    @Override
    public List<RawMaterial> getAll() {
        ArrayList<RawMaterial> list = new ArrayList();
        String sql = "SELECT * FROM rawmaterial";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RawMaterial rawmaterial = RawMaterial.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public RawMaterial save(RawMaterial obj) {
        String sql = "INSERT INTO rawmaterial (rm_name, rm_minimum, rm_unit_price, rm_qoh)"
                + "VALUES(?, ?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getRawmaterialName());
            stmt.setInt(2, obj.getMinimum());
            stmt.setFloat(3, obj.getUnitPrice());
            stmt.setInt(4, obj.getQoh());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setRawmaterialId(id);
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public RawMaterial update(RawMaterial obj) {
        String sql = "UPDATE rawmaterial"
                + " SET rm_name = ?, rm_minimum = ?, rm_unit_price=?, rm_qoh=?, rm_indate = datetime('now','localtime')"
                + " WHERE rm_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getRawmaterialName());
            stmt.setInt(2, obj.getMinimum());
            stmt.setFloat(3, obj.getUnitPrice());
            stmt.setInt(4, obj.getQoh());
            stmt.setInt(5, obj.getRawmaterialId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(RawMaterial obj) {
        String sql = "DELETE FROM rawmaterial WHERE rm_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getRawmaterialId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    @Override
    public List<RawMaterial> getAll(String where, String order) {
        ArrayList<RawMaterial> list = new ArrayList();
        String sql = "SELECT * FROM rawmaterial WHERE " + where + " ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RawMaterial rawmaterial = RawMaterial.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<RawMaterial> getAll(String order) {
        ArrayList<RawMaterial> list = new ArrayList();
        String sql = "SELECT * FROM rawmaterial ORDER BY " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                RawMaterial rawmaterial = RawMaterial.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public RawMaterial getByName(String name) {
        RawMaterial material = null;
        String sql = "SELECT * FROM rawmaterial WHERE rm_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();
            
            while(rs.next()){
               material = RawMaterial.fromRS(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RawmaterialDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return material;
    }
    
}
