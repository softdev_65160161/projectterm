/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;


import helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Vendor;

/**
 *
 * @author 65160
 */
public class VendorDao implements Dao<Vendor> {

    @Override
    public Vendor get(int id) {
        Vendor vendor = null;
        String sql = "SELECT * FROM vendor WHERE vendor_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                vendor = Vendor.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return vendor;
    }

    @Override
    public List<Vendor> getAll() {
        ArrayList<Vendor> list = new ArrayList();
        String sql = "SELECT * FROM vendor";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Vendor rawmaterial = Vendor.fromRS(rs);
                list.add(rawmaterial);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Vendor save(Vendor obj) {
        String sql = "INSERT INTO vendor(vendor_name, vendor_contract, vendor_phone)"
                + "VALUES(?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getContract());
            stmt.setString(3, obj.getPhone());

            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Vendor update(Vendor obj) {
        String sql = "UPDATE vendor"
                + " SET vendor_name = ?, vendor_contract = ?, vendor_phone=?"
                + " WHERE vendor_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getContract());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Vendor obj) {
        String sql = "DELETE FROM vendor WHERE vendor_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;  
    }

    @Override
    public List<Vendor> getAll(String where, String order) {
        String sql = "SELECT * FROM vendor WHERE "+where+" ORDER BY "+order;
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<Vendor> list = new ArrayList<>();
        
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                Vendor vendor = Vendor.fromRS(rs);
                list.add(vendor);
            }
                      
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
      return list;
    }

}
