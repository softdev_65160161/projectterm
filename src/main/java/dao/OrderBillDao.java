/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import model.OrderBill;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.OrderBillDetail;

/**
 *
 * @author 65160
 */
public class OrderBillDao implements Dao<OrderBill>{

    @Override
    public OrderBill get(int id) {
        OrderBill orderBill = null;
        String sql = "SELECT * FROM order_bill WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                orderBill = OrderBill.fromRS(rs);
                OrderBillDetailDao orderBillDetailDao = new OrderBillDetailDao();
                ArrayList<OrderBillDetail> orderBillDetails = (ArrayList<OrderBillDetail>) orderBillDetailDao.getAll();
                orderBill.setOrderBillDetial(orderBillDetails);  
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return orderBill;
    }

    @Override
    public List<OrderBill> getAll() {
        OrderBill orderBill = null;
        String sql = "SELECT * FROM order_bill";
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<OrderBill> list = new ArrayList();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                orderBill = OrderBill.fromRS(rs);
                list.add(orderBill);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    
    }

    @Override
    public OrderBill save(OrderBill obj) {
        String sql = "INSERT INTO order_bill(order_total, order_tax, vendor_name, emp_id) VALUES(?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1,obj.getTotal());
            stmt.setFloat(2,obj.getTax());
            stmt.setString(3,obj.getVendorName());
            stmt.setInt(4,obj.getEmpId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;  
    }

    @Override
    public OrderBill update(OrderBill obj) {
        String sql = "UPDATE order_bill"
                + " SET order_total = ?"
                + " WHERE  order_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getTotal());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(OrderBill obj) {
        String sql = "DELETE FROM order_bill Where order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
            
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public List<OrderBill> getAll(String where, String order) {
        OrderBill orderBill = null;
        String sql = "SELECT * FROM order_bill WHERE "+where+" ORDER BY "+order;
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<OrderBill> list = new ArrayList();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                orderBill = OrderBill.fromRS(rs);
                list.add(orderBill);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
}
