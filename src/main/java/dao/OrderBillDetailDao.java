/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;


import helper.DatabaseHelper;
import model.OrderBillDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author 65160
 */
public class OrderBillDetailDao implements Dao<OrderBillDetail>{

    @Override
    public OrderBillDetail get(int id) {
        OrderBillDetail orderBillDetail = null;
        String sql = "SELECT * FROM order_bill_detail_detail WHERE orbd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                orderBillDetail = OrderBillDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return orderBillDetail;
    }

    @Override
    public List<OrderBillDetail> getAll() {
        OrderBillDetail orderBillDetail = null;
        String sql = "SELECT * FROM order_bill_detail";
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<OrderBillDetail> list = new ArrayList();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                orderBillDetail = OrderBillDetail.fromRS(rs);
                list.add(orderBillDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    
    }

    @Override
    public OrderBillDetail save(OrderBillDetail obj) {
        String sql = "INSERT INTO order_bill_detail(orbd_unit_price, orbd_qty, orbd_net,  rm_id, order_id) VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();

        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1,obj.getUnitPrice());
            stmt.setFloat(2,obj.getQty());
            stmt.setFloat(3,obj.getNet());
            stmt.setInt(4, obj.getRawMaterialId());
            stmt.setInt(5, obj.getOrderId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDetailDao.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return obj;  
    }

    @Override
    public OrderBillDetail update(OrderBillDetail obj) {
        String sql = "UPDATE order_bill_detail"
                + " SET orbd_qty = ?, orbd_net=?, rm_id=?, order_id=?"
                + " WHERE  orbd_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setFloat(1, obj.getQty());
            stmt.setFloat(2, obj.getNet());
            stmt.setFloat(3, obj.getRawMaterialId());
            stmt.setFloat(4, obj.getOrderId());
            stmt.setInt(5,obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(OrderBillDetail obj) {
        String sql = "DELETE FROM order_bill_detail Where orbd_id=?";
        Connection conn = DatabaseHelper.getConnect();
        
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
            
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    @Override
    public List<OrderBillDetail> getAll(String where, String order) {
        OrderBillDetail orderBillDetail = null;
        String sql = "SELECT * FROM order_bill_detail WHERE "+where+" ORDER BY "+order;
        Connection conn = DatabaseHelper.getConnect();
        ArrayList<OrderBillDetail> list = new ArrayList();

        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()){
                orderBillDetail = OrderBillDetail.fromRS(rs);
                list.add(orderBillDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderBillDetailDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
    
}
