/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import helper.DatabaseHelper;
import model.CostOfUtilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author natta
 */
public class CostOfUtilitiesDao implements Dao<CostOfUtilities> {

    @Override
    public CostOfUtilities get(int id) {
        CostOfUtilities costOfUtilities = null;
        String sql = "SELECT * FROM cost_of_utilities WHERE cou_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                costOfUtilities = CostOfUtilities.fromRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return costOfUtilities;
    }

    @Override
    public List<CostOfUtilities> getAll() {
        ArrayList<CostOfUtilities> list = new ArrayList();
        String sql = "SELECT * FROM cost_of_utilities";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CostOfUtilities costOfUtilities = CostOfUtilities.fromRS(rs);
                list.add(costOfUtilities);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<CostOfUtilities> getAll(String where, String order) {
        ArrayList<CostOfUtilities> list = new ArrayList();
        String sql = "SELECT * FROM cost_of_utilities WHERE " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CostOfUtilities costOfUtilities = CostOfUtilities.fromRS(rs);
                list.add(costOfUtilities);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CostOfUtilities> getAll(String order) {
        ArrayList<CostOfUtilities> list = new ArrayList();
        String sql = "SELECT * FROM cost_of_utilities  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CostOfUtilities costOfUtilities = CostOfUtilities.fromRS(rs);
                list.add(costOfUtilities);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CostOfUtilities save(CostOfUtilities obj) {

        String sql = "INSERT INTO cost_of_utilities (cou_status, cou_total_price, cou_type, cou_unit, emp_id)"
                + "VALUES(?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getStatus());
            stmt.setFloat(2, obj.getTotalPrice());
            stmt.setInt(3, obj.getType());
            stmt.setInt(4, obj.getUnit());
            stmt.setInt(5, obj.getEmpId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CostOfUtilities update(CostOfUtilities obj) {
        String sql = "UPDATE cost_of_utilities"
                + " SET cou_status = ?, cou_total_price = ?, cou_type, emp_id = ? "
                + " WHERE cou_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getStatus());
            stmt.setFloat(2, obj.getTotalPrice());
            stmt.setInt(3, obj.getEmpId());
            stmt.setInt(4, obj.getType());
            stmt.setInt(5, obj.getUnit());
            stmt.setInt(6, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CostOfUtilities obj) {
        String sql = "DELETE FROM cost_of_utilities WHERE cou_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
